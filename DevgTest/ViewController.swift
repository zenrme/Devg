//
//  ViewController.swift
//  DevgTest
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

import Devg
import DevgAd
import DevgKeyboard
import DevgPlayer

class ViewController: UIViewController, DevgKeyboardDelegate {
    var keyboard: DevgKeyboard!
    
    @IBOutlet var zoomImageView: DevgZoomView!

    override func viewDidLoad() {
        super.viewDidLoad()
        l(Devg.languageCode())
        l(Devg.timezone())
        l(Devg.uuid())
        l(DevgAd.uuid())
        
        DevgHUD.sharedInstance.show()
        DevgHUD.sharedInstance.hide(delay: 2)
        
        self.keyboard = DevgKeyboard(target: self)
        
        UIImageView().setImageFromUrl("https://pbs.twimg.com/media/ChcSG0lU0AACpP7.jpg", completion: { (image, isCache) in
            self.zoomImageView.image = image
        }) { (error) in
            e(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    @IBAction func date(sender: UIButton) {
        DevgPickerView.sharedInstance.showDate("날짜") { (date) in
            d(date)
        }
    }
    
    // MARK: - Devg keyboard delegate
    func keyboardChangeStatus(notification: NSNotification, status: DevgKeyboard.KeyboardStatus, size: CGSize) {
        
    }
}

