//
//  DevgExtensionUIImage.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

public extension UIImage {
    public func aspectFit(size: CGSize) -> UIImage? {
        var newImage: UIImage?
        let originalWidth = self.size.width
        let originalHeight = self.size.height
        var scaleFactor: CGFloat = 0.0
        var newSize: CGSize
        
        if size.width > 0 && size.height > 0 {
            if originalWidth > originalHeight {
                scaleFactor = size.width / originalWidth
            }
            else {
                scaleFactor = size.height / originalHeight
            }
        }
        else {
            if size.width > 0 {
                scaleFactor = size.width / originalWidth
            }
            else if size.height > 0 {
                scaleFactor = size.height / originalHeight
            }
            else {
                e("Resize and Crop Failed. \(size)")
                return nil
            }
        }
        
        newSize = CGSize(width: originalWidth * scaleFactor, height: originalHeight * scaleFactor)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.drawInRect(CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        if newImage == nil {
            e("Resize and Crop Failed. \(size)")
        }
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public func aspectFill(size: CGSize) -> UIImage? {
        let sourceImage: UIImage = self
        var newImage: UIImage?
        let imageSize: CGSize = sourceImage.size
        let width: CGFloat = imageSize.width
        let height: CGFloat = imageSize.height
        let targetWidth: CGFloat = size.width
        let targetHeight = size.height
        var scaleFactor: CGFloat = 0.0
        var scaledWidth: CGFloat = targetWidth
        var scaledHeight: CGFloat = targetHeight
        var thumbnailPoint = CGPoint(x: 0.0, y: 0.0)
        
        if !CGSizeEqualToSize(imageSize, size) {
            let widthFactor: CGFloat = targetWidth / width
            let heightFactor: CGFloat = targetHeight / height
            
            if widthFactor > heightFactor {
                scaleFactor = widthFactor
            }
            else {
                scaleFactor = heightFactor
            }
            
            scaledWidth = width * scaleFactor
            scaledHeight = height * scaleFactor
            
            if widthFactor > heightFactor {
                thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5
            }
            else {
                if widthFactor < heightFactor {
                    thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5
                }
            }
        }
        
        UIGraphicsBeginImageContext(size)
        
        var thumbnailRect: CGRect = CGRectZero
        thumbnailRect.origin = thumbnailPoint
        thumbnailRect.size.width = scaledWidth
        thumbnailRect.size.height = scaledHeight
        
        sourceImage.drawInRect(thumbnailRect)
        
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        
        if newImage == nil {
            e("Resize and Crop Failed. \(size)")
        }
        
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public func widthFit(width: CGFloat) -> UIImage? {
        var newImage: UIImage?
        let ratio = width / self.size.width
        let height = self.size.height * ratio
        var newSize: CGSize
        newSize = CGSize(width: width, height: height)
        UIGraphicsBeginImageContext(newSize)
        self.drawInRect(CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        newImage = UIGraphicsGetImageFromCurrentImageContext()
        if newImage == nil {
        }
        UIGraphicsEndImageContext()
        return newImage
    }
}
