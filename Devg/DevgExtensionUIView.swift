//
//  DevgExtensionUIView.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

public extension UIView {
    public func showView(duration: NSTimeInterval = 0) {
        self.hidden = false
        if duration > 0 {
            self.alpha = 0
            UIView.animateWithDuration(duration, animations: { () -> Void in
                self.alpha = 1
            })
        }
        else {
            self.alpha = 1
        }
    }
    
    public func hideView(duration: NSTimeInterval = 0) {
        if duration > 0 {
            UIView.animateWithDuration(duration, animations: { () -> Void in
                self.alpha = 0
                }, completion: { (finish) -> Void in
                    self.hidden = true
            })
        }
        else {
            self.hidden = true
        }
    }
    
    public func addSubview(view: UIView, topConstant: CGFloat?, leftConstant: CGFloat?, rightConstant: CGFloat?, bottomConstant: CGFloat?) {
        self.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        if let top: CGFloat = topConstant {
            let topConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: top)
            self.addConstraint(topConstraint)
        }
        if let left: CGFloat = leftConstant {
            let leftConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Leading, multiplier: 1, constant: left)
            self.addConstraint(leftConstraint)
        }
        if let right: CGFloat = rightConstant {
            let rightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Trailing, multiplier: 1, constant: right)
            self.addConstraint(rightConstraint)
        }
        if let bottom: CGFloat = bottomConstant {
            let bottomConstraint: NSLayoutConstraint = NSLayoutConstraint(item: view, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: bottom)
            self.addConstraint(bottomConstraint)
        }
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func topConstant(constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutAttribute = .Bottom) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let topConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(topConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func leftConstant(constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutAttribute = .Trailing) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let leftConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Leading, relatedBy: NSLayoutRelation.Equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(leftConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func rightConstant(constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutAttribute = .Leading) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let rightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Trailing, relatedBy: NSLayoutRelation.Equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(rightConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func bottomConstant(constant: CGFloat = 0, toView: UIView, toAttribute: NSLayoutAttribute = .Top) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let bottomConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: toView, attribute: toAttribute, multiplier: 1, constant: constant)
        self.superview?.addConstraint(bottomConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func widthConstant(constant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let widthConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: constant)
        self.addConstraint(widthConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
    
    public func heightConstant(constant: CGFloat) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let heightConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: constant)
        self.addConstraint(heightConstraint)
        
        self.setNeedsLayout()
        self.superview?.setNeedsLayout()
    }
}