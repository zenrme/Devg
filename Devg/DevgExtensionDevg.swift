//
//  DevgExtensionDevg.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public extension Devg {
    /// iOS 버전 정보.
    ///
    /// :returns: Float
    public class func osVersion() -> String {
        return UIDevice.currentDevice().systemVersion
    }
    
    public class func timezone() -> String {
        let secondsGMT: Int = NSTimeZone.localTimeZone().secondsFromGMT
        let plus: String = (secondsGMT < 0) ? "-" : "+"
        let minutes: Int = (secondsGMT / 60) % 60
        let hours: Int = (secondsGMT / 60) / 60
        let string: String = plus + String(format: "%02d", hours) + ":" + String(format: "%02d", minutes)
        return string
    }
    
    public class func languageCode() -> String {
        guard let language: String = NSLocale.preferredLanguages()[0] else {
            return "en"
        }
        
        if let languageCode: String = NSLocale.componentsFromLocaleIdentifier(language)["kCFLocaleLanguageCodeKey"] {
            return languageCode
        }
        else {
            return "en"
        }
    }
    
    /// 앱 스토어의 앱 버전 정보.
    ///
    /// :returns: Void
    public class func appVersionFromAppStore(completion: ((version: String) -> Void), error: ((error: NSError) -> Void)) {
        guard let url = NSURL(string: "http://itunes.apple.com/lookup?bundleId=\(Devg.bundelID())") else {
            error(error: NSError(domain: "", code: 404, userInfo: nil))
            return
        }
        let request = NSURLRequest(URL: url)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {(response, data, err) in
            if err == nil {
                guard let object: AnyObject = String(data: data!, encoding:NSUTF8StringEncoding)?.toJSON() else {
                    error(error: NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let array: [AnyObject] = object.objectForKey("results") as? [AnyObject] else {
                    error(error: NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let dictionary: [String: AnyObject] = array.get(0) as? [String: AnyObject] else {
                    error(error: NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                guard let version: String = dictionary["version"] as? String else {
                    error(error: NSError(domain: "", code: 500, userInfo: nil))
                    return
                }
                
                completion(version: version)
            }
            else {
                error(error: err!)
            }
        }
    }
    
    /// UUID
    ///
    /// :returns: String
    public class func uuid() -> String {
        var uuid: String?
        let keyName: String = "UUID"
        
        let SecMatchLimit: String = kSecMatchLimit as String
        let SecReturnData: String = kSecReturnData as String
        let SecValueData: String = kSecValueData as String
        let SecAttrAccessible: String = kSecAttrAccessible as String
        let SecClass: String = kSecClass as String
        let SecAttrService: String = kSecAttrService as String
        let SecAttrGeneric: String = kSecAttrGeneric as String
        let SecAttrAccount: String = kSecAttrAccount as String
        
        var readKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
        readKeychainQuery[SecAttrService] = Devg.bundelID()
        let encodedIdentifier: NSData? = keyName.dataUsingEncoding(NSUTF8StringEncoding)
        readKeychainQuery[SecAttrGeneric] = encodedIdentifier
        readKeychainQuery[SecAttrAccount] = encodedIdentifier
        
        var result: AnyObject?
        readKeychainQuery[SecMatchLimit] = kSecMatchLimitOne
        readKeychainQuery[SecReturnData] = kCFBooleanTrue
        let status = withUnsafeMutablePointer(&result) {
            SecItemCopyMatching(readKeychainQuery, UnsafeMutablePointer($0))
        }
        let keychainData: NSData? = status == noErr ? result as? NSData : nil
        var stringValue: String?
        if let data = keychainData {
            stringValue = NSString(data: data, encoding: NSUTF8StringEncoding) as String?
        }
        
        uuid = stringValue
        
        if uuid == nil { // create
            let newUuid: String = UIDevice.currentDevice().identifierForVendor!.UUIDString
            
            var saveKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
            saveKeychainQuery[SecAttrService] = Devg.bundelID()
            let encodedIdentifier: NSData? = keyName.dataUsingEncoding(NSUTF8StringEncoding)
            saveKeychainQuery[SecAttrGeneric] = encodedIdentifier
            saveKeychainQuery[SecAttrAccount] = encodedIdentifier
            
            saveKeychainQuery[SecValueData] = newUuid.dataUsingEncoding(NSUTF8StringEncoding)
            saveKeychainQuery[SecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            
            uuid = newUuid
        }
        
        return uuid!
    }
    
    // TODO: - AppDelegate 해결
    public class func visibleViewController() -> UIViewController {
        var visibleViewController: UIViewController = UIViewController()
        if let rootViewController: UIViewController = UIApplication.sharedApplication().delegate!.window!!.rootViewController {
            visibleViewController = self.getVisibleWithPresented(rootViewController)
        }
        return visibleViewController
    }
    
    private class func getVisibleWithPresented(presented: UIViewController) -> UIViewController {
        if presented.presentedViewController == nil {
            if presented is UINavigationController {
                let navigation: UINavigationController = presented as! UINavigationController
                let last: UIViewController = navigation.viewControllers.last!
                
                return self.getVisibleWithPresented(last)
            }
            else if presented is UITabBarController {
                let tabBar: UITabBarController = presented as! UITabBarController
                if let seleted: UIViewController = tabBar.selectedViewController {
                    return self.getVisibleWithPresented(seleted)
                }
                else {
                    return UIViewController()
                }
            }
            else {
                return presented
            }
        }
        else if presented.presentedViewController is UINavigationController {
            let navigation: UINavigationController = presented.presentedViewController as! UINavigationController
            let last: UIViewController = navigation.viewControllers.last!
            
            return self.getVisibleWithPresented(last)
        }
        else if presented.presentedViewController is UITabBarController {
            let tabBar: UITabBarController = presented.presentedViewController as! UITabBarController
            if let seleted: UIViewController = tabBar.selectedViewController {
                return self.getVisibleWithPresented(seleted)
            }
            else {
                return self.getVisibleWithPresented(UIViewController())
            }
        }
        else {
            return self.getVisibleWithPresented(presented.presentedViewController as UIViewController!)
        }
    }
    
    // MARK: - Web
    
    /// 사파리로 이동.
    public class func safari(url: String) -> Bool {
        var aUrl: String
        if !url.isEmpty {
            aUrl = url
        }
        else {
            return false
        }
        
        if !url.isRegex("://") && !url.isRegex("tel:") {
            if url.isPhoneNumber(.KoreanPhone) {
                aUrl = "telprompt://" + url
            }
            else {
                aUrl = "http://" + url
            }
        }
        if let bUrl: NSURL = NSURL(string: aUrl) {
            UIApplication.sharedApplication().openURL(bUrl)
            return true
        }
        else {
            return false
        }
    }
}