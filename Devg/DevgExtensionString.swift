//
//  DevgExtensionString.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public enum PhoneNumberType {
    case KoreanCell
    case KoreanPhone
}

public enum PhoneNumberSecretType {
    case None
    case Half
    case All
}

public enum NumberFormatType {
    case ThousandSeparator
    case Dollar
}

public enum PasswordType {
    case Normal
    case KoreanGovernment
    case KoreanGovernmentLow
}

public enum URLEncodeType {
    case Normal
    case Full
}

public extension String {
    public func unwrap() -> String {
        if self.hasPrefix("Optional(") && self.hasSuffix(")") {
            return self.substring(9, length: self.characters.count - 10)
        }
        else {
            return self
        }
    }
    
    public func toJSON() -> AnyObject? {
        var json: AnyObject? = nil
        if let data = self.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                try json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers)
            }
            catch let error {
                e(error)
            }
        }
        
        return json
    }
    
    public func toBool() -> Bool {
        var bool: Bool = false
        let trueStrings: [String] = ["true", "yes", "1", "y"]
        if trueStrings.contains(self.lowercaseString) {
            bool = true
        }
        
        return bool
    }
    
    public func toInt() -> Int {
        var response: Int = 0
        if let int: Int = Int(self) {
            response = int
        }
        
        return response
    }
    
    public func toFloat() -> Float {
        var response: Float = 0.0
        let array: [String] = self.componentsSeparatedByString(".")
        if array.count > 1 {
            let before = Int(array[0])
            if before == nil {
                return response
            }
            else {
                response = Float(before!)
                let after = Int(array[1])
                if after == nil {
                    return response
                }
                else {
                    let length = array[1].characters.count
                    var point: Float = Float(after!)
                    for _ in 0..<length {
                        point = point * 0.1
                    }
                    response = response + point
                }
            }
        }
        else {
            let all = Int(self)
            if all == nil {
                return response
            }
            else {
                response = Float(all!)
            }
        }
        
        return response
    }
    
    public func substring(start: Int, length: Int) -> String {
        var newStart = start
        if newStart < 0 {
            newStart = self.characters.count + newStart
        }
        let from = self.substringFromIndex(self.startIndex.advancedBy(newStart))
        
        return from.substringToIndex(from.startIndex.advancedBy(length))
    }
    
    public func toPhoneNumberFormat(type: PhoneNumberType, secretType: PhoneNumberSecretType = .None) -> String {
        var phoneNumber: String = ""
        var formatPhoneNumber = ""
        
        for char in [Character](self.characters) {
            let charToString = String(char)
            if Int(charToString) != nil {
                phoneNumber += charToString
            }
        }
        
        let phoneNumberCount: Int = phoneNumber.characters.count
        formatPhoneNumber = phoneNumber
        
        switch type {
        case .KoreanCell:
            var separate1 = ""
            var separate2: String
            var separate3: String
            if phoneNumberCount > 3 {
                separate1 = phoneNumber.substring(0, length: 3)
                separate2 = phoneNumber.substring(3, length: phoneNumberCount - 3)
                
                formatPhoneNumber = "\(separate1)-\(separate2)"
            }
            
            if phoneNumberCount > 3 + 4 {
                separate2 = phoneNumber.substring(3, length: 4)
                separate3 = phoneNumber.substring(3 + 4, length: phoneNumberCount - (3 + 4))
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            
            if phoneNumberCount == 3 + 3 + 4 {
                separate2 = phoneNumber.substring(3, length: 3)
                separate3 = phoneNumber.substring(3 + 3, length: 4)
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            break
        case .KoreanPhone:
            var separate1Length = 3
            if phoneNumber.hasPrefix("02") {
                separate1Length = 2
            }
            
            var separate1 = ""
            var separate2: String
            var separate3: String
            if phoneNumberCount > separate1Length {
                separate1 = phoneNumber.substring(0, length: separate1Length)
                separate2 = phoneNumber.substring(separate1Length, length: phoneNumberCount - separate1Length)
                
                formatPhoneNumber = "\(separate1)-\(separate2)"
            }
            
            if phoneNumberCount > separate1Length + 4 {
                separate2 = phoneNumber.substring(separate1Length, length: 4)
                separate3 = phoneNumber.substring(separate1Length + 4, length: phoneNumberCount - (separate1Length + 4))
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            
            if phoneNumberCount == separate1Length + 3 + 4 {
                separate2 = phoneNumber.substring(separate1Length, length: 3)
                separate3 = phoneNumber.substring(separate1Length + 3, length: 4)
                
                formatPhoneNumber = "\(separate1)-\(separate2)-\(separate3)"
            }
            break
        }
        
        if secretType != .None {
            let numberSeparates: [String] = formatPhoneNumber.componentsSeparatedByString("-")
            if numberSeparates.count > 1 {
                var newSeparate2: String
                if secretType == .Half {
                    newSeparate2 = numberSeparates[1].substring(0, length: 2)
                    let separateCount: Int = numberSeparates[1].characters.count - 2
                    for _ in 0..<separateCount {
                        newSeparate2 = newSeparate2 + "*"
                    }
                }
                else {
                    newSeparate2 = ""
                    let separateCount: Int = numberSeparates[1].characters.count
                    for _ in 0..<separateCount {
                        newSeparate2 = newSeparate2 + "*"
                    }
                }
                
                formatPhoneNumber = numberSeparates[0] + "-" + newSeparate2
                if numberSeparates.count > 2 {
                    formatPhoneNumber = formatPhoneNumber + "-" + numberSeparates[2]
                }
            }
        }
        
        return formatPhoneNumber
    }
    
    public func toNumberFormat(type: NumberFormatType = .ThousandSeparator) -> String {
        let formatter = NSNumberFormatter()
        
        switch type {
        case .Dollar:
            formatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
            formatter.currencyCode = "USD"
            formatter.currencySymbol = ""
        default:
            formatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        }
        
        var numberString: String?
        if let toInt = Float(self) {
            if let formatString = formatter.stringFromNumber(toInt) {
                numberString = formatString
            }
        }
        
        guard let returnString: String = numberString else {
            return ""
        }
        
        return returnString
    }
    
    public func toBase64() -> String {
        let plainData = (self as NSString).dataUsingEncoding(NSUTF8StringEncoding)
        let base64Data = plainData?.base64EncodedDataWithOptions([])
        
        var returnString = ""
        if let data = base64Data {
            returnString = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
        }
        
        return returnString
    }
    
    public func fromBase64() -> String {
        let decodedData: NSData? = NSData(base64EncodedString: self, options: [])
        
        var returnString: String = ""
        if let data: NSData = decodedData {
            returnString = NSString(data: data, encoding: NSUTF8StringEncoding) as! String
        }
        
        return returnString
    }
    
    public func isSizeMinimum(minimum: Int, maximum: Int? = nil) -> Bool {
        var isAllow: Bool = true
        
        if self.characters.count < minimum {
            isAllow = false
        }
        
        if maximum != nil {
            if self.characters.count > maximum {
                isAllow = false
            }
        }
        
        return isAllow
    }
    
    public func isEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: [], range: NSMakeRange(0, self.characters.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isPhoneNumber(type: PhoneNumberType) -> Bool {
        return self.isRegex("^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-?[0-9]{3,4}-?[0-9]{4}$")
    }
    
    public func isSignId() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z]{1}[A-Z0-9]", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: [], range: NSMakeRange(0, self.characters.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isRegex(pattern: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            return regex.firstMatchInString(self, options: [], range: NSMakeRange(0, self.characters.count)) != nil
        }
        catch {
            return false
        }
    }
    
    public func isPassword(type: PasswordType = .Normal) -> Bool {
        var isAllow = false
        isAllow = self.isRegex("(?=.*[a-zA-Z]|.*[!@#$%^&*?+=_-~]|.*[0-9]).{1,100}$")
        
        if (type == .KoreanGovernment || type == .KoreanGovernmentLow) && isAllow {
            var checkCount: Int = 0
            if type == .KoreanGovernment {
                checkCount = 3
            }
            else if type == .KoreanGovernmentLow {
                checkCount = 2
            }
            var isCount = 0
            
            if self.isRegex("[a-z]") {
                isCount += 1
            }
            
            if self.isRegex("[A-Z]") {
                isCount += 1
            }
            
            if self.isRegex("[0-9]") {
                isCount += 1
            }
            
            if self.isRegex("[!,@,#,$,%,^,&,*,?,+,=,_,\\-,~]") {
                isCount += 1
            }
            
            if isCount >= checkCount {
                isAllow = true
            }
            else {
                isAllow = false
            }
        }
        
        return isAllow
    }
    
    /// URL Encode.
    ///
    /// :returns: Encoded string for URL
    public func toURLEncode(type: URLEncodeType = .Normal) -> String? {
        var characterSet: NSCharacterSet
        switch type {
        case .Normal:
            characterSet = .URLQueryAllowedCharacterSet()
            break
        case .Full:
            characterSet = .URLUserAllowedCharacterSet()
        }
        if let encoded: String = self.stringByAddingPercentEncodingWithAllowedCharacters(characterSet) {
            return encoded
        }
        else {
            return nil
        }
    }
    
    /// URL Decode.
    ///
    /// :returns: Normal string.
    public func toURLDecode() -> String? {
        if let normal = self.stringByRemovingPercentEncoding {
            return normal
        }
        else {
            return nil
        }
    }
    
    /// toDate.
    ///
    /// :param: 포멧 String
    /// :returns: Normal string.
    public func toDate(format: String) -> NSDate? {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.dateFromString(self)
    }
    
    /// localized.
    ///
    /// 반드시 "Localizable"로 파일명을 정해야 한다.
    /// :returns: 나라설정에 맞는 언어로 번역하여 제공
    public func localized() -> String {
        var language: String = "en"
        if let currentLanguage = NSUserDefaults.standardUserDefaults().objectForKey("LCLCurrentLanguageKey") as? String {
            language = currentLanguage
        }
        
        guard let preferredLanguage = NSBundle.mainBundle().preferredLocalizations.first else {
            return self
        }
        
        let availableLanguages: [String] = NSBundle.mainBundle().localizations
        if (availableLanguages.contains(preferredLanguage)) {
            language = preferredLanguage
        }
        
        if let path = NSBundle.mainBundle().pathForResource(language, ofType: "lproj"), bundle = NSBundle(path: path) {
            return bundle.localizedStringForKey(self, value: nil, table: nil)
        }
        else if let path = NSBundle.mainBundle().pathForResource("Base", ofType: "lproj"), bundle = NSBundle(path: path) {
            return bundle.localizedStringForKey(self, value: nil, table: nil)
        }
        return self
    }
}