//
//  DevgZoomView.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 3. 25..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

@IBDesignable public class DevgZoomView: UIView, UIScrollViewDelegate {
    public var imageView: UIImageView = UIImageView()
    public var imageScrollView: UIScrollView = UIScrollView()
    
    @IBInspectable public var image: UIImage? {
        didSet {
            self.imageView.image = self.image
        }
    }
    
    @IBInspectable public var minimumZoomScale: CGFloat = 1.0 {
        didSet {
            self.imageScrollView.minimumZoomScale = self.minimumZoomScale
        }
    }
    
    @IBInspectable public var maximumZoomScale: CGFloat = 2.0 {
        didSet {
            self.imageScrollView.maximumZoomScale = self.maximumZoomScale
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.setup()
    }
    
    func setup() {
        if self.gestureRecognizers?.count == 0 || self.gestureRecognizers == nil {
            let doubleTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(DevgZoomView.doubleTapGesture(_:)))
            doubleTap.numberOfTapsRequired = 2
            self.imageView.addGestureRecognizer(doubleTap)
            self.imageView.userInteractionEnabled = true
            self.imageView.frame = self.frame
            self.imageView.contentMode = self.contentMode
            self.imageScrollView.frame = self.frame
            self.imageScrollView.delegate = self
            self.imageScrollView.addSubview(self.imageView)
            self.imageScrollView.scrollsToTop = false
            
            self.addSubview(self.imageScrollView)
            
            let topScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            let leftScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .Leading, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Leading, multiplier: 1, constant: 0)
            let bottomScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Bottom, multiplier: 1, constant: 0)
            let rightScrollViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageScrollView, attribute: .Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Trailing, multiplier: 1, constant: 0)
            
            self.addConstraints([topScrollViewConstraint, leftScrollViewConstraint, bottomScrollViewConstraint, rightScrollViewConstraint])
            
            let topImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .Top, relatedBy: NSLayoutRelation.Equal, toItem: self.imageScrollView, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
            let leftImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .Leading, relatedBy: NSLayoutRelation.Equal, toItem: self.imageScrollView, attribute: .Leading, multiplier: 1, constant: 0)
            let bottomImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self.imageScrollView, attribute: .Bottom, multiplier: 1, constant: 0)
            let rightImageViewConstraint: NSLayoutConstraint = NSLayoutConstraint(item: self.imageView, attribute: .Trailing, relatedBy: NSLayoutRelation.Equal, toItem: self.imageScrollView, attribute: .Trailing, multiplier: 1, constant: 0)
            
            self.imageScrollView.addConstraints([topImageViewConstraint, leftImageViewConstraint, bottomImageViewConstraint, rightImageViewConstraint])
        }
    }
    
    func doubleTapGesture(gestureRecognizer: UITapGestureRecognizer) {
        if self.imageScrollView.zoomScale == 1.0 {
            self.imageScrollView.zoomToRect(self.zoomRect(self.maximumZoomScale, center: gestureRecognizer.locationInView(gestureRecognizer.view)), animated: true)
        }
        else {
            self.imageScrollView.zoomToRect(self.zoomRect(self.minimumZoomScale, center: gestureRecognizer.locationInView(gestureRecognizer.view)), animated: true)
        }
    }
    
    func zoomToScale(scale: CGFloat) {
        self.imageScrollView.zoomToRect(self.zoomRect(scale, center: self.imageView.center), animated: true)
    }
    
    func zoomRect(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect: CGRect = self.imageScrollView.frame
        zoomRect.size.height = self.imageScrollView.frame.height / scale
        zoomRect.size.width = self.imageScrollView.frame.width / scale
        zoomRect.origin.x = center.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = center.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    // MARK: - UIScrollViewDelegate
    public func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
}