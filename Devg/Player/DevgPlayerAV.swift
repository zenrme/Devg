//
//  DevgPlayerAV.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 5. 4..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Devg


public protocol DevgPlayerAVDelegate {
    func av(av: AVPlayer, changePlaybackState state: AVPlayerItemStatus, error: NSError?)
    func av(av: AVPlayer, playbackFinish: AVPlayerItemAccessLog?)
    func av(av: AVPlayer, accessLogWithLog: AVPlayerItemAccessLog?)
    func av(av: AVPlayer, errorLogWithLog: AVPlayerItemErrorLog?, error: NSError?)
}

public class DevgPlayerAV: NSObject {
    var isUserStop: Bool = false
    var resumeTime: Int?
    //    var av: AVPlayerViewController = AVPlayerViewController()
    var playerUrl: NSURL = NSURL()
    public var delegate: DevgPlayerAVDelegate?
    var checkPlayingTimer: NSTimer?
    var av: AVPlayer?
    var avItem: AVPlayerItem!
    var isFile: Bool = false
    
    public struct Duration {
        public var current: Int
        public var total: Int
    }
    
    
    // MARK: - Overrides
    override init() {
        super.init()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgPlayerAV.newAccessLogEntry(_:)), name: AVPlayerItemNewAccessLogEntryNotification, object: self.avItem)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgPlayerAV.newErrorLogEntry(_:)), name: AVPlayerItemNewErrorLogEntryNotification, object: self.avItem)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgPlayerAV.failedToPlayToEndTime(_:)), name: AVPlayerItemFailedToPlayToEndTimeNotification, object: self.avItem)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgPlayerAV.didPlayToEndTime(_:)), name: AVPlayerItemDidPlayToEndTimeNotification, object: self.avItem)
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
        }
        catch let error {
            e(error)
        }
        
        UIApplication.sharedApplication().beginReceivingRemoteControlEvents()
    }
    
    
    // MARK: - Functions
    public func play() {
        self.isUserStop = false
        
        if let player = self.av {
            if self.resumeTime != nil {
                let t = CMTime(seconds: Double(self.resumeTime!), preferredTimescale: 1000000000)
                player.seekToTime(t)
                self.resumeTime = nil
            }
            player.play()
            guard let item: AVPlayerItem = player.currentItem else {
                return
            }
            if self.playerUrl.absoluteString.rangeOfString(".m3u8") != nil {
                self.delegate?.av(player, changePlaybackState: item.status, error: item.error)
                self.delegate?.av(player, accessLogWithLog: item.accessLog())
            }
        }
        else {
            self.avItem = AVPlayerItem(URL: self.playerUrl)
            if let avItems = self.avItem {
                self.av = AVPlayer(playerItem: avItems)
                if let avs = self.av {
                    avs.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.New, context: nil)      // 상태가 알수 없음, 재생준비 일때와, 유효하지 않은 주소를 참조했을때 발생하는 에러도 이곳으로 타게
                }
            }
        }
        
        if self.isFile == true {
            guard let player: AVPlayer = self.av else {
                return
            }
            guard let item: AVPlayerItem = player.currentItem else {
                return
            }
            
            self.delegate?.av(player, changePlaybackState: item.status, error: item.error)
            self.delegate?.av(player, accessLogWithLog: item.accessLog())
        }
    }
    
    override public func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if keyPath == "status" {
            if let player = object as? AVPlayer {
                if player.status == .Failed {
                    self.delegate?.av(player, errorLogWithLog: player.currentItem?.errorLog(), error: player.currentItem?.error)
                }
                else if player.status == .ReadyToPlay {
                    if self.resumeTime != nil {
                        let t = CMTime(seconds: Double(self.resumeTime!), preferredTimescale: 1000000000)
                        player.seekToTime(t)
                        self.resumeTime = nil
                    }
                    player.play()
                    if self.isFile == true {
                        guard let player: AVPlayer = self.av else {
                            return
                        }
                        guard let item: AVPlayerItem = player.currentItem else {
                            return
                        }
                        
                        self.delegate?.av(player, changePlaybackState: item.status, error: item.error)
                        self.delegate?.av(player, accessLogWithLog: item.accessLog())
                    }
                }
                else if player.status == .Unknown {
                }
            }
        }
    }
    
    public func isPlaying() -> Bool {
        guard let player: AVPlayer = self.av else {
            return false
        }
        if player.rate == 0 {
            return false
        }
        else {
            return true
        }
    }
    
    public func pause() {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        player.pause()
        if self.playerUrl.absoluteString.rangeOfString(".m3u8") != nil {
            self.delegate?.av(player, changePlaybackState: item.status, error: item.error)
            self.delegate?.av(player, accessLogWithLog: item.accessLog())
        }
    }
    
    public func stop() {
        if let player = self.av {
            self.isUserStop = true
            player.pause()
            player.removeObserver(self, forKeyPath: "status", context: nil)
            self.av = nil
        }
    }
    
    func timeToJump(jump: Double) {
        if let player = self.av {
            let t: CMTime = CMTime(seconds: jump, preferredTimescale: 1000000000)
            player.seekToTime(t)
        }
    }
    
    func timeToSeek(seek: Double) {
        if let player = self.av {
            player.currentTime()
            let t: CMTime = CMTime(seconds: seek, preferredTimescale: 1000000000)
            let resultT = CMTimeAdd(player.currentTime(), t)
            player.seekToTime(resultT)
        }
    }
    
    func currentSpeed() -> Float {
        if let player = self.av {
            return player.rate
        }
        return 1
    }
    
    func changeSpeed(rate: Float) {
        if let player = self.av {
            player.rate = rate
        }
    }
    
    public func duration() -> DevgPlayerAV.Duration {
        if let player = self.av {
            if !isnan(player.currentTime().seconds) && !isnan(player.currentItem!.asset.duration.seconds) {
                return DevgPlayerAV.Duration(current: Int(player.currentTime().seconds), total: Int(player.currentItem!.asset.duration.seconds))
            }
        }
        return DevgPlayerAV.Duration(current: 0, total: 0)
    }
    
    public func setContentURL(urlString: String, isStreaming: Bool) {
        self.setContentURL(urlString, isStreaming: isStreaming, resumeTime: nil)
    }
    
    public func setContentURL(urlString: String, isStreaming: Bool, resumeTime: Int?) {
        self.resumeTime = resumeTime
        
        if let player = self.av {
            if ((player.rate != 0) && (player.error == nil)) {
                self.stop()
            }
        }
        
        if isStreaming {
            self.isFile = false
            if let url: NSURL = NSURL(string: urlString) {
                self.playerUrl = url
            }
        }
        else {
            self.isFile = true
            self.playerUrl = NSURL(fileURLWithPath: urlString)
        }
    }
    
    func checkPlaying() {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        if player.status == .Unknown || player.rate == 0 {
            self.stop()
            self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error)
        }
    }
    
    // MARK: - Notifications
    func newAccessLogEntry(notification: NSNotification) {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        
        self.delegate?.av(player, changePlaybackState: item.status, error: item.error)
        self.delegate?.av(player, accessLogWithLog: item.accessLog())
    }
    
    func newErrorLogEntry(notification: NSNotification) {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        guard let log = item.errorLog() else {
            return
        }
        guard let event = log.events.get(0) else {
            return
        }
        if event.errorStatusCode != -12318 {
            self.stop()
            self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error)
        }
    }
    
    func failedToPlayToEndTime(notification: NSNotification) {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        self.stop()
        self.delegate?.av(player, errorLogWithLog: item.errorLog(), error: item.error)
    }
    
    func didPlayToEndTime(notification: NSNotification) {
        guard let player: AVPlayer = self.av else {
            return
        }
        guard let item: AVPlayerItem = player.currentItem else {
            return
        }
        self.delegate?.av(player, playbackFinish: item.accessLog())
    }
    
}