//
//  DevgPlayer.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MediaPlayer
import Devg
import CoreTelephony

public protocol DevgPlayerDelegate {
    func player(player: DevgPlayer, changeStatus status: DevgPlayerStatus.Enum)
    func player(player: DevgPlayer, duration: DevgPlayerAV.Duration)
    func player(player: DevgPlayer, playStartQueue queue: DevgPlayerQueue.Struct?)
    func player(player: DevgPlayer, playEndQueue queue: DevgPlayerQueue.Struct?)
    func player(player: DevgPlayer, removedCurrentQueue queue: DevgPlayerQueue.Struct?)
}

public class DevgPlayer: NSObject, DevgPlayerAVDelegate, DevgPlayerNetworkDelegate, DevgPlayerQueueDelegate {
    public var backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    public var delegate: DevgPlayerDelegate?
    public var timer: NSTimer?
    public var playingQueue: DevgPlayerQueue.Struct?
    
    public var status: DevgPlayerStatus = DevgPlayerStatus()
    public var queue: DevgPlayerQueue = DevgPlayerQueue()
    public var network: DevgPlayerNetwork = DevgPlayerNetwork()
    public var av: DevgPlayerAV = DevgPlayerAV()
    
    public var isResume: Bool = false
    public var callCenter = CTCallCenter()
    public var isCall: Bool = false
    
    public class var sharedInstance: DevgPlayer {
        struct Static {
            static var token : dispatch_once_t = 0
            static var instance : DevgPlayer! = nil
        }
        dispatch_once(&Static.token) {
            Static.instance = DevgPlayer()
        }
        return Static.instance!
    }
    
    public override init() {
        super.init()
        MPRemoteCommandCenter.sharedCommandCenter().skipBackwardCommand.enabled = false
        MPRemoteCommandCenter.sharedCommandCenter().seekBackwardCommand.enabled = false
        MPRemoteCommandCenter.sharedCommandCenter().skipForwardCommand.enabled = false
        MPRemoteCommandCenter.sharedCommandCenter().seekForwardCommand.enabled = false
        self.network.delegate = self
        self.av.delegate = self
        self.queue.delegate = self
    }
    
    // MARK: - Functions
    // MARK: settings
    public func setRepeatType(repeatType: DevgPlayerQueue.RepeatType) {
        self.queue.repeatType = repeatType
    }
    
    public func currentRepeatType() -> DevgPlayerQueue.RepeatType {
        return self.queue.repeatType
    }
    
    public func setResumeMode(bool: Bool) {
        self.isResume = bool
    }
    
    func currentResumeMode() -> Bool {
        return self.isResume
    }
    
    // MARK: control
    public func play() {
        var allow: Bool = false
        if self.network.isForcePlay {
            allow = true
        }
        else if self.network.allowPlay() {
            allow = true
        }
        
        if allow {
            self.network.isForcePlay = false
            if self.playingQueue == nil {
                if let queue: DevgPlayerQueue.Struct = self.queue.current() {
                    self.playingQueue = queue
                    
                    if queue.isStreaming {
                        self.status.setCurrent(DevgPlayerStatus.Enum.Start)
                        self.delegate?.player(self, changeStatus: self.status.current())
                        self.delegate?.player(self, playStartQueue: self.queue.current())
                    }
                    
                    var resumeTime: Int? = nil
                    if self.isResume {
                        resumeTime = queue.resumeTime
                    }
                    self.av.setContentURL(queue.url, isStreaming: queue.isStreaming,resumeTime: resumeTime)
                }
            }
            self.av.play()
        }
        else {
            self.status.setCurrent(DevgPlayerStatus.Enum.Stop)
            self.delegate?.player(self, changeStatus: self.status.current())
        }
    }
    
    public func pause() {
        self.status.setCurrent(DevgPlayerStatus.Enum.Pause)
        self.delegate?.player(self, changeStatus: self.status.current())
        
        if self.timer != nil {
            self.timer!.invalidate()
            self.timer = nil
        }
        if self.backgroundTask != UIBackgroundTaskInvalid {
            self.endBackgroundTask()
        }
        
        self.av.pause()
    }
    
    public func stop(resetIndex: Bool = false) {
        if self.status.current() != DevgPlayerStatus.Enum.Stop {
            self.playingQueue = nil
            self.status.setCurrent(DevgPlayerStatus.Enum.Stop)
            self.delegate?.player(self, changeStatus: self.status.current())
            
            if self.timer != nil {
                self.timer!.invalidate()
                self.timer = nil
            }
            if self.backgroundTask != UIBackgroundTaskInvalid {
                self.endBackgroundTask()
            }
            
            self.av.stop()
        }
    }
    
    public func newPlay() {
        self.playingQueue = nil
        self.play()
    }
    
    public func next() {
        if self.queue.repeatType == .Normal && self.queue.index + 1 >= self.queue.queues.count {
            self.stop()
        }
        else {
            self.queue.index = self.queue.nextQueueIndex()
            self.newPlay()
        }
    }
    
    public func previous() {
        if self.queue.repeatType == .Normal && self.queue.index - 1 < 0 {
            self.stop()
        }
        else {
            self.queue.index = self.queue.previousQueueIndex()
            self.newPlay()
        }
    }
    
    public func currentSpeed() -> Float {
        return self.av.currentSpeed()
    }
    
    public func changeSpeed(rate: Float) {
        if rate > 0.0 {
            self.av.changeSpeed(rate)
        }
    }
    
    public func timeToJump(jump: Double) {
        self.av.timeToJump(jump)
    }
    
    public func timeToSeek(seek: Double) {
        self.av.timeToSeek(seek)
    }
    
    public func singlePlayNewQueue(queue: DevgPlayerQueue.Struct) {
        self.stop()
        self.resetQueues()
        self.addQueue(queue)
        self.newPlay()
    }
    
    public func singlePlayVideoOnFullScreen(url: String) {
        self.stop()
        
        if NSFileManager.defaultManager().fileExistsAtPath(url) {
            guard let fileUrl: NSURL = NSURL(fileURLWithPath: url) else {
                return
            }
            
            let avViewController: AVPlayerViewController = AVPlayerViewController()
            avViewController.showsPlaybackControls = false
            avViewController.player = AVPlayer(URL: fileUrl)
            Devg.visibleViewController().presentViewController(avViewController, animated: true, completion: nil)
        }
        else {
            var aUrl: String = url
            if !url.isRegex("://") {
                aUrl = "http://" + url
            }
            
            if let bUrl: NSURL = NSURL(string: aUrl) {
                let avViewController: AVPlayerViewController = AVPlayerViewController()
                avViewController.showsPlaybackControls = false
                avViewController.player = AVPlayer(URL: bUrl)
                Devg.visibleViewController().presentViewController(avViewController, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: status
    public func currentStatus() -> DevgPlayerStatus.Enum {
        return self.status.current()
    }
    
    // MARK: playlist
    public func currentQueue() -> DevgPlayerQueue.Struct? {
        return self.queue.current()
    }
    
    public func playQueue(queue: DevgPlayerQueue.Struct) {
        if let findIndex = self.queue.findIndex(queue) {
            self.setIndex(findIndex)
            self.newPlay()
        }
    }
    
    public func playIndex(index: Int) {
        self.setIndex(index)
        self.newPlay()
    }
    
    public func syncQueueIndex(queue: DevgPlayerQueue.Struct) {
        if let findIndex: Int = self.queue.findIndex(queue) {
            self.queue.index = findIndex
        }
    }
    
    public func addQueue(queue: DevgPlayerQueue.Struct) {
        self.queue.add(queue)
    }
    
    public func removeQueueIndex(index: Int) {
        self.queue.removeAtIndex(index)
    }
    
    public func removeQueue(queue: DevgPlayerQueue.Struct) {
        if let findIndex: Int = self.queue.findIndex(queue) {
            self.removeQueueIndex(findIndex)
        }
    }
    
    public func changeIndex(index: Int, queue: DevgPlayerQueue.Struct) {
        self.queue.changeIndex(index, queue: queue)
    }
    
    public func setIndex(index: Int, beforeStop isStop: Bool = true) {
        if isStop {
            self.stop()
        }
        self.queue.index = index
    }
    
    public func setIndexWithQueue(queue: DevgPlayerQueue.Struct, beforeStop isStop: Bool = true) {
        guard let findIndex: Int = self.queue.findIndex(queue) else {
            return
        }
        
        self.setIndex(findIndex, beforeStop: isStop)
    }
    
    public func setQueues(queues: [DevgPlayerQueue.Struct], currentIndex index: Int = 0, beforeStop isStop: Bool = true) {
        self.queue.queues = queues
        self.setIndex(index, beforeStop: isStop)
    }
    
    public func resetQueues() {
        self.queue.reset()
    }
    
    public func queuesCount() -> Int {
        return self.queue.queues.count
    }
    
    // MARK: network
    public func setAllowNetwork(allowNetwork: DevgPlayerNetwork.Allow) {
        self.network.allow = allowNetwork
    }
    
    // MARK: etc
    public func timeLoop() {
        guard let player: AVPlayer = self.av.av else {
            return
        }
        
        self.callCenter.callEventHandler = { (call:CTCall!) in
            switch call.callState {
            case CTCallStateConnected, CTCallStateDialing, CTCallStateIncoming:
                self.callConnected()
            case CTCallStateDisconnected:
                self.callDisconnected()
            default:
                break
            }
        }
        
        if self.isResume {
            if !isnan(player.currentTime().seconds) {
                self.queue.setResumeTime(Int(player.currentTime().seconds), atIndex: self.queue.index)
            }
        }
        
        if var songInfo: [String : AnyObject] = MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo as [String : AnyObject]? {
            songInfo[MPNowPlayingInfoPropertyElapsedPlaybackTime] = NSNumber(int: Int32(player.currentTime().seconds))
            songInfo[MPMediaItemPropertyPlaybackDuration] = NSNumber(int: Int32(player.currentItem!.asset.duration.seconds))
            MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
        }
        
        self.delegate?.player(self, duration: self.av.duration())
    }
    
    public func lockScreenWithQueue(queue: DevgPlayerQueue.Struct?) {
        guard let songQueue: DevgPlayerQueue.Struct = queue else {
            return
        }
        
        if NSClassFromString("MPNowPlayingInfoCenter") == nil {
            return
        }
        
        var isNew: Bool = true
        if let nowInfo: [String: AnyObject] = MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo {
            if String(nowInfo[MPMediaItemPropertyTitle]).unwrap() == songQueue.title && String(nowInfo[MPMediaItemPropertyArtist]).unwrap() == songQueue.artist && String(nowInfo[MPMediaItemPropertyAlbumTitle]).unwrap() == songQueue.album {
                isNew = false
            }
        }
        
        if isNew {
            var songInfo: [String: AnyObject] = [MPMediaItemPropertyTitle: songQueue.title]
            if let string: String = songQueue.artist {
                songInfo[MPMediaItemPropertyArtist] = string
            }
            if let string: String = songQueue.album {
                songInfo[MPMediaItemPropertyAlbumTitle] = string
            }
            
            if let image: UIImage = songQueue.albumArt as? UIImage {
                songInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: image)
                MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
            }
            else if let string: String = songQueue.albumArt as? String {
                UIImageView().setImageFromUrl(string, defaultImage: UIImage(), completion: { (image, isCache) -> Void in
                    songInfo[MPMediaItemPropertyArtwork] = MPMediaItemArtwork(image: image)
                    MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
                    }, error: { (error) -> Void in
                        e(error)
                        MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
                })
            }
            else {
                MPNowPlayingInfoCenter.defaultCenter().nowPlayingInfo = songInfo
            }
        }
    }
    
    public func registerBackgroundTask() {
        self.backgroundTask = UIApplication.sharedApplication().beginBackgroundTaskWithExpirationHandler {
            [unowned self] in
            self.endBackgroundTask()
        }
        assert(self.backgroundTask != UIBackgroundTaskInvalid)
    }
    
    public func endBackgroundTask() {
        UIApplication.sharedApplication().endBackgroundTask(self.backgroundTask)
        self.backgroundTask = UIBackgroundTaskInvalid
    }
    
    // MARK: - Player Network delegate
    public func wifiToCellular() {
        if self.status.current() == DevgPlayerStatus.Enum.Playing {
            switch self.network.allow {
            case .OnlyWifi, .QuestionCellular:
                var isDeny: Bool = true
                if let queue: DevgPlayerQueue.Struct = self.playingQueue {
                    if !queue.isStreaming {
                        isDeny = false
                    }
                }
                
                if isDeny {
                    if #available(iOS 8.3, *) {
                        let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "Wi-Fi 연결이 3G/4G 로 변경되어 종료되었습니다.", preferredStyle: .Alert)
                        let cancelAction: UIAlertAction = UIAlertAction(title: "확인", style: .Cancel, handler: nil)
                        alert.addAction(cancelAction)
                        Devg.visibleViewController().presentViewController(alert, animated: true, completion: nil)
                    }
                    else {
                        let alertView = UIAlertView(title: "네트워크 연결", message: "Wi-Fi 연결이 3G/4G 로 변경되어 종료되었습니다.", delegate: nil, cancelButtonTitle: "확인")
                        alertView.show()
                    }
                    self.pause()
                }
                break;
            default:
                break;
            }
        }
    }
    
    public func forcePlay() {
        self.network.isForcePlay = true
        self.play()
    }
    
    /* in AppDelegate.swift
     override func remoteControlReceivedWithEvent(event: UIEvent?) {
     Player.sharedInstance.remoteControlReceivedWithEvent(event)
     }
     */
    public func remoteControlReceivedWithEvent(event: UIEvent?) {
        if let resiveEvent: UIEvent = event {
            if resiveEvent.type == UIEventType.RemoteControl {
                switch resiveEvent.subtype {
                case .RemoteControlPlay:
                    self.play()
                case .RemoteControlStop:
                    self.stop()
                case .RemoteControlPause:
                    self.pause()
                case .RemoteControlPreviousTrack:
                    self.previous()
                case .RemoteControlNextTrack:
                    self.next()
                case .RemoteControlTogglePlayPause:
                    if self.currentStatus() == DevgPlayerStatus.Enum.Playing {
                        self.pause()
                    }
                    else {
                        self.play()
                    }
                default:
                    break
                }
            }
        }
    }
    
    // MARK: - Player AV delegate
    public func av(av: AVPlayer, changePlaybackState state: AVPlayerItemStatus, error: NSError?) {
        if let _: NSError = error {
            if #available(iOS 8.3, *) {
                let alert: UIAlertController = UIAlertController(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", preferredStyle: .Alert)
                let okAction: UIAlertAction = UIAlertAction(title: "확인", style: .Cancel, handler: nil)
                alert.addAction(okAction)
                Devg.visibleViewController().presentViewController(alert, animated: true, completion: nil)
            }
            else {
                UIAlertView(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", delegate: nil, cancelButtonTitle: "확인").show()
            }
            self.stop()
        }
        
    }
    
    public func av(av: AVPlayer, accessLogWithLog: AVPlayerItemAccessLog?) {
        if ((av.rate != 0) && (av.error == nil)) {
            self.status.setCurrent(DevgPlayerStatus.Enum.Playing)
            
            if self.timer == nil {
                self.timeLoop()
                self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(DevgPlayer.timeLoop), userInfo: nil, repeats: true)
            }
            if self.backgroundTask == UIBackgroundTaskInvalid {
                self.registerBackgroundTask()
            }
            
            self.delegate?.player(self, changeStatus: self.status.current())
        } else {
            self.status.setCurrent(DevgPlayerStatus.Enum.Pause)
            self.delegate?.player(self, changeStatus: self.status.current())
        }
    }
    
    public func av(av: AVPlayer, errorLogWithLog: AVPlayerItemErrorLog?, error: NSError?) {
        if #available(iOS 8.3, *) {
            let alert: UIAlertController = UIAlertController(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", preferredStyle: .Alert)
            let okAction: UIAlertAction = UIAlertAction(title: "확인", style: .Cancel, handler: nil)
            alert.addAction(okAction)
            Devg.visibleViewController().presentViewController(alert, animated: true, completion: nil)
        }
        else {
            UIAlertView(title: "", message: "해당 미디어를 재생할 수 없습니다. 네트워크 상태나 미디어의 상태를 확인해 주세요.", delegate: nil, cancelButtonTitle: "확인").show()
        }
        self.stop()
    }
    
    public func av(av: AVPlayer, playbackFinish: AVPlayerItemAccessLog?) {
        self.delegate?.player(self, playEndQueue: self.queue.current())
        self.next()
    }
    
    // MARK: - Player Queue delegate
    public func queue(queue: DevgPlayerQueue, removedCurrentQueue removedQueue: DevgPlayerQueue.Struct) {
        self.stop()
        self.delegate?.player(self, removedCurrentQueue: removedQueue)
    }
    
    // MARK: -  CTCallCenterEventHandler
    public func callConnected() {
        if let player = self.av.av {
            if player.status != .Unknown || player.rate != 0 {
                self.isCall = true
                self.pause()
            }
        }
    }
    
    public func callDisconnected() {
        if let player = self.av.av {
            if self.isCall && player.rate == 0 {
                self.isCall = false
                self.play()
            }
        }
    }
}
