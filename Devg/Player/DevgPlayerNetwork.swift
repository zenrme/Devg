//
//  DevgPlayerNetwork.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit
import Devg

public protocol DevgPlayerNetworkDelegate {
    func wifiToCellular()
    func forcePlay()
}

public class DevgPlayerNetwork: NSObject, UIAlertViewDelegate {
    public enum Allow: Int {
        case All
        case OnlyWifi
        case LostWifi
        case QuestionCellular
    }
    
    public enum Connection: Int {
        case NotConnected
        case Wifi
        case Cellular
    }
    
    public var isForcePlay: Bool = false
    
    public var delegate: DevgPlayerNetworkDelegate?
    public var connection: DevgPlayerNetwork.Connection = DevgPlayerNetwork.Connection.NotConnected
    
    internal var allow: DevgPlayerNetwork.Allow = DevgPlayerNetwork.Allow.QuestionCellular
    private var reachability: Reachability!
    
    // MARK: - Overrides
    public override init() {
        super.init()
        
        do {
            self.reachability = try Reachability.reachabilityForInternetConnection()
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgPlayerNetwork.reachabilityChanged(_:)),name: ReachabilityChangedNotification,object: reachability)
            
            reachability.whenReachable = { reachability in
                dispatch_async(dispatch_get_main_queue()) {
                    if reachability.isReachableViaWiFi() {
                        self.connection = .Wifi
                    } else {
                        if self.connection == .Wifi {
                            self.delegate?.wifiToCellular()
                        }
                        
                        self.connection = .Cellular
                    }
                }
            }
            reachability.whenUnreachable = { reachability in
                dispatch_async(dispatch_get_main_queue()) {
                    self.connection = .NotConnected
                }
            }
            
            if self.reachability.isReachable() {
                if self.reachability.isReachableViaWiFi() {
                    self.connection = .Wifi
                } else {
                    self.connection = .Cellular
                }
            }
            
            do {
                try self.reachability.startNotifier()
            }
            catch let error {
                e(error)
            }
        }
        catch let error {
            e(error)
        }
    }
    
    deinit {
        self.reachability.stopNotifier()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: reachability)
    }
    
    // MARK: - Functions
    public func allowPlay() -> Bool {
        var allowPlay: Bool = true
        if self.connection == .Cellular {
            switch self.allow {
            case .OnlyWifi, .LostWifi:
                if #available(iOS 8.3, *) {
                    let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "3G/4G 환경에서 재생하실 수 없습니다.", preferredStyle: .Alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "확인", style: .Cancel, handler: nil)
                    alert.addAction(cancelAction)
                    Devg.visibleViewController().presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    UIAlertView(title: "네트워크 연결", message: "3G/4G 환경에서 재생하실 수 없습니다.", delegate: nil, cancelButtonTitle: "확인").show()
                }
                allowPlay = false
                break;
            case .QuestionCellular:
                if #available(iOS 8.3, *) {
                    let alert: UIAlertController = UIAlertController(title: "네트워크 연결", message: "3G/4G 환경입니다. 데이타요금이 발생할 수 있습니다. 재생하시겠습니까?", preferredStyle: .Alert)
                    let cancelAction: UIAlertAction = UIAlertAction(title: "취소", style: .Cancel, handler: nil)
                    alert.addAction(cancelAction)
                    let playAction: UIAlertAction = UIAlertAction(title: "재생", style: .Default, handler: { (action) -> Void in
                        self.delegate?.forcePlay()
                        self.isForcePlay = false
                    })
                    alert.addAction(playAction)
                    Devg.visibleViewController().presentViewController(alert, animated: true, completion: nil)
                }
                else {
                    let alertView = UIAlertView(title: "네트워크 연결", message: "3G/4G 환경입니다. 데이타요금이 발생할 수 있습니다. 재생하시겠습니까?", delegate: self, cancelButtonTitle: "취소", otherButtonTitles: "재생")
                    alertView.tag = 1
                    alertView.show()
                }
                allowPlay = false
                break;
            default:
                break;
            }
        } else if self.connection == .NotConnected {
            DevgAlert.show(withViewController: Devg.visibleViewController(), title: "네트워크 연결", message: "인터넷 연결상태를 확인 해 주세요.", buttons: ["확인"], destructiveIndex: nil, handler: { (buttonIndex) in
            })
            allowPlay = false
        }
        
        return allowPlay
    }
    
    // MARK: - Alert View Delegate
    public func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        switch alertView.tag {
        case 1:
            if buttonIndex == 1 {
                self.delegate?.forcePlay()
                self.isForcePlay = false
            }
            break;
        default:
            break;
        }
    }
    
    func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                if reachability.isReachableViaWiFi() {
                    self.connection = .Wifi
                } else {
                    if self.connection == .Wifi {
                        self.delegate?.wifiToCellular()
                    }
                }
            } else {
                self.connection = .Cellular
            }
        } else {
            self.connection = .NotConnected
            
        }
    }
}
