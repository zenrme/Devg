//
//  DevgPlayerStatus.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public class DevgPlayerStatus: NSObject {
    public enum Enum: Int {
        case Start
        case Playing
        case Pause
        case Stop
        case Error
    }
    
    public var index: Int = 2
    
    // MARK: - Overrides
    override init() {
        super.init()
        
        self.setCurrent(DevgPlayerStatus.Enum.Stop)
    }
    
    // MARK: - Functions
    public func current() -> DevgPlayerStatus.Enum {
        return DevgPlayerStatus.Enum(rawValue: self.index)!
    }
    
    public func setCurrent(status: DevgPlayerStatus.Enum) {
        self.index = status.rawValue
    }
}
