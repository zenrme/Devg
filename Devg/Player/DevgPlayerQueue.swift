//
//  DevgPlayerQueue.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 10..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public protocol DevgPlayerQueueDelegate {
    func queue(queue: DevgPlayerQueue, removedCurrentQueue removedQueue: DevgPlayerQueue.Struct)
}

public class DevgPlayerQueue: NSObject {
    public struct Struct {
        public var trackNo: String
        public var trackMax: String? = nil
        public var title: String
        public var artist: String? = nil
        public var album: String? = nil
        public var albumArt: AnyObject? = nil
        public var url: String
        public var resumeTime: Int?
        public var isVideo: Bool
        public var isStreaming: Bool
    }
    
    public enum RepeatType: Int {
        case Normal
        case All
        case One
    }
    
    public var delegate: DevgPlayerQueueDelegate?
    public var queues: [DevgPlayerQueue.Struct] = Array<DevgPlayerQueue.Struct>()
    public var index: Int = 0
    public var repeatType: DevgPlayerQueue.RepeatType = DevgPlayerQueue.RepeatType.Normal
    
    // MARK: - Functions
    public class func create(trackNo trackNo: String, trackMax: String?, title: String, artist: String?, album: String?, albumArt: AnyObject?, url: String, resumeTime: Int?, isVideo: Bool, isStreaming: Bool) -> DevgPlayerQueue.Struct {
        return DevgPlayerQueue.Struct(trackNo: trackNo, trackMax: trackMax, title: title, artist: artist, album: album, albumArt: albumArt, url: url, resumeTime: resumeTime, isVideo: isVideo, isStreaming: isStreaming)
    }
    
    public func current() -> DevgPlayerQueue.Struct? {
        return self.queueAtIndex(self.index)
    }
    
    public func add(queue: DevgPlayerQueue.Struct) {
        self.queues.append(queue)
    }
    
    public func add(queue: DevgPlayerQueue.Struct, toIndex index: Int) {
        self.queues.insert(queue, atIndex: index)
    }
    
    public func remove(queue: DevgPlayerQueue.Struct) {
        if let findIndex = self.findIndex(queue) {
            self.queues.removeAtIndex(findIndex)
        }
    }
    
    public func removeAtIndex(index: Int) {
        if let removeQueue: DevgPlayerQueue.Struct = self.queueAtIndex(index) {
            self.queues.removeAtIndex(index)
            
            if index == self.index {
                if self.index != 0 && self.index >= self.queues.count {
                    self.index = self.queues.count - 1
                }
                
                self.delegate?.queue(self, removedCurrentQueue: removeQueue)
            }
        }
    }
    
    public func setResumeTime(resumeTime: Int?, atIndex index: Int) {
        if var queue = self.queues.get(index) {
            if let time = resumeTime {
                queue.resumeTime = time
            }
            else {
                queue.resumeTime = 0
            }
        }
    }
    
    public func currentChangeAlbumArt(imageString: String) {
        if var queue = self.queues.get(self.index) {
            queue.albumArt = imageString
        }
    }
    
    public func queueAtIndex(index: Int) -> DevgPlayerQueue.Struct? {
        if index < self.queues.count && index >= 0 {
            if var queue = self.queues.get(index) {
                return queue
            }
            return nil
        }
        else {
            return nil
        }
    }
    
    public func changeIndex(index: Int, queue: DevgPlayerQueue.Struct) {
        if let fromIndex: Int = self.findIndex(queue) {
            if fromIndex != index {
                if let queue = self.queues.get(fromIndex) {
                    if let fromQueue: DevgPlayerQueue.Struct = queue {
                        var currentQueue: DevgPlayerQueue.Struct?
                        if let queue: DevgPlayerQueue.Struct = self.current() {
                            currentQueue = queue
                        }
                        
                        if fromIndex < index {
                            self.queues.removeAtIndex(fromIndex)
                            self.queues.insert(fromQueue, atIndex: index)
                        }
                        else {
                            self.queues.insert(fromQueue, atIndex: index)
                            self.queues.removeAtIndex(fromIndex + 1)
                        }
                        
                        if let queue: DevgPlayerQueue.Struct = currentQueue {
                            if let findIndex = self.findIndex(queue) {
                                self.index = findIndex
                            }
                        }
                    }
                }
            }
        }
    }
    
    public func reset() {
        if self.queues.count > 0 {
            self.queues.removeAll()
        }
        self.index = 0
    }
    
    public func findIndex(queue: DevgPlayerQueue.Struct) -> Int? {
        var i: Int = -1
        for findQueue: DevgPlayerQueue.Struct in self.queues {
            i = i + 1
            var isFind: Bool = true
            
            if queue.trackNo != findQueue.trackNo {
                isFind = false
                continue
            }
            if queue.trackMax != findQueue.trackMax {
                isFind = false
                continue
            }
            if queue.title != findQueue.title {
                isFind = false
                continue
            }
            if queue.artist != findQueue.artist {
                isFind = false
                continue
            }
            if queue.album != findQueue.album {
                isFind = false
                continue
            }
            if queue.url != findQueue.url {
                isFind = false
                continue
            }
            if queue.isVideo != findQueue.isVideo {
                isFind = false
                continue
            }
            if queue.isStreaming != findQueue.isStreaming {
                isFind = false
                continue
            }
            
            if isFind {
                return i
            }
        }
        
        return nil
    }
    
    public func nextQueueIndex() -> Int {
        var nextIndex: Int = self.index + 1
        switch self.repeatType {
        case .Normal:
            if nextIndex >= self.queues.count {
                nextIndex = self.index
            }
            break
        case .All:
            if nextIndex >= self.queues.count {
                nextIndex = 0
            }
        case .One:
            nextIndex = self.index
        }
        
        return nextIndex
    }
    
    public func previousQueueIndex() -> Int {
        var previousIndex: Int = self.index - 1
        switch self.repeatType {
        case .Normal:
            if previousIndex < 0 {
                previousIndex = 0
            }
            break
        case .All:
            if previousIndex < 0 {
                previousIndex = self.queues.count - 1
            }
        case .One:
            previousIndex = self.index
        }
        
        return previousIndex
    }
}
