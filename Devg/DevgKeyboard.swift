//
//  DevgKeyboard.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 3. 14..
//  Copyright © 2016년 Kwan. All rights reserved.
//
import Foundation
import UIKit

public protocol DevgKeyboardDelegate {
    func keyboardChangeStatus(notification: NSNotification, status: DevgKeyboard.KeyboardStatus, size: CGSize)
}

public class DevgKeyboard: NSObject {
    public enum KeyboardStatus: Int {
        case WillShow
        case DidShow
        case WillHide
        case DidHide
    }
    
    var textItemArray: [AnyObject] = Array<AnyObject>()
    var toolbarArray: [UIToolbar] = Array<UIToolbar>()
    var targetView: UIView!
    var defaultColor: UIColor = UIColor(red: 98/255, green: 172/255, blue: 255/255, alpha: 1)
    var defaultNonColor: UIColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1)
    var delegate: DevgKeyboardDelegate? = nil
    
    override init() {
        
    }
    
    public convenience init(target: DevgKeyboardDelegate) {
        self.init()
        self.delegate = target
        if let targetViewController: UIViewController = target as? UIViewController {
            self.targetView = targetViewController.view
        }
        self.registerDevgKeyboard()
    }
    
    // MARK: - register
    func registerDevgKeyboard() {
        self.textItemArray.removeAll()
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
//        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidChangeFrameNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgKeyboard.keyboardStatus(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgKeyboard.keyboardStatus(_:)), name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgKeyboard.keyboardStatus(_:)), name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgKeyboard.keyboardStatus(_:)), name: UIKeyboardDidHideNotification, object: nil)
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardChangeFrame:", name: UIKeyboardDidChangeFrameNotification, object: nil)
        self.addTextItemArray(self.targetView)
        self.itemsSetAccessoryView()
    }
    
    
    func unRegisterDevgKeyboard() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidHideNotification, object: nil)
//        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardDidChangeFrameNotification, object: nil)
    }
    
    // MARK: - function
    func addTextItemArray(view: UIView) {
        
        let subView: [UIView] = view.subviews
        if subView.count == 0 {
            return
        }
        
        for object: UIView in subView {
            if let textField: UITextField = object as? UITextField {
                self.textItemArray.append(textField)
            }
            if let textView: UITextView = object as? UITextView {
                self.textItemArray.append(textView)
            }
            self.addTextItemArray(object)
        }
    }
    
    func itemsSetAccessoryView() {
        for (index, textItem) in self.textItemArray.enumerate() {
            let prevButton: UIButton = UIButton(type: .System)
            prevButton.frame = CGRectMake(0, 0, 20, 30)
            prevButton.setTitle("<", forState: .Normal)
            if #available(iOS 8.2, *) {
                prevButton.titleLabel?.font = UIFont.systemFontOfSize(30, weight: UIFontWeightThin)
            } else {
                prevButton.titleLabel?.font = UIFont.systemFontOfSize(30)
            }
            prevButton.setTitleColor(self.defaultColor, forState: .Normal)
            prevButton.addTarget(self, action: #selector(DevgKeyboard.prevButtonPush(_:)), forControlEvents: .TouchUpInside)
            prevButton.tag = index
            
            let nextButton: UIButton = UIButton(type: .System)
            nextButton.frame = CGRectMake(10, 0, 30, 30)
            nextButton.setTitle(">", forState: .Normal)
            if #available(iOS 8.2, *) {
                nextButton.titleLabel?.font = UIFont.systemFontOfSize(30, weight: UIFontWeightThin)
            } else {
                nextButton.titleLabel?.font = UIFont.systemFontOfSize(30)
            }
            nextButton.setTitleColor(self.defaultColor, forState: .Normal)
            nextButton.addTarget(self, action: #selector(DevgKeyboard.nextButtonPush(_:)), forControlEvents: .TouchUpInside)
            nextButton.tag = index
            
            let centerView: UIView = UIView(frame: CGRectMake(0,0,36,4.5))
            centerView.layer.cornerRadius = 2.5
            centerView.layer.masksToBounds = true
            centerView.backgroundColor = UIColor.clearColor()
            
            
            let prevButtonItem: UIBarButtonItem = UIBarButtonItem(customView: prevButton)
            let nextButtonItem: UIBarButtonItem = UIBarButtonItem(customView: nextButton)
            let centerButtinItem: UIBarButtonItem = UIBarButtonItem(customView: centerView)
            let doneButtonItem: UIBarButtonItem = UIBarButtonItem(title: "완료", style: UIBarButtonItemStyle.Done, target: self, action: #selector(DevgKeyboard.doneButtonPush(_:)))
            let spaceButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            let fixButtonItem: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
            fixButtonItem.width = 30
            doneButtonItem.width = 20
            doneButtonItem.tag = index
            
            
            let toolBar: UIToolbar = UIToolbar()
            toolBar.barStyle = UIBarStyle.Default
            toolBar.translucent = true
            toolBar.tintColor = self.defaultColor
            toolBar.setItems([prevButtonItem, nextButtonItem, spaceButtonItem, centerButtinItem, spaceButtonItem, fixButtonItem, doneButtonItem], animated: false)
            toolBar.sizeToFit()
            
            self.toolbarArray.append(toolBar)
//            let swipeDown: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRecognizer:")
//            swipeDown.direction = .Down
//            toolBar.addGestureRecognizer(swipeDown)
//            
//            let swipeUp: UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: "swipeRecognizer:")
//            swipeUp.direction = .Up
//            toolBar.addGestureRecognizer(swipeUp)
            
            if index == self.textItemArray.count - 1 {
                nextButtonItem.enabled = false
                nextButton.setTitleColor(self.defaultNonColor, forState: .Normal)
            }
            if index == 0 {
                prevButtonItem.enabled = false
                prevButton.setTitleColor(self.defaultNonColor, forState: .Normal)
            }
            
            
            if let field: UITextField = textItem as? UITextField {
                field.inputAccessoryView = toolBar
            }
            if let view: UITextView = textItem as? UITextView {
                view.inputAccessoryView = toolBar
            }
            
            
        }
    }
    
//    func showAndHideToolBar(toolbar: UIToolbar, isUp: Bool) {
//        UIView.animateWithDuration(0.2, animations: { () -> Void in
//            if isUp == true {
//                if toolbar.frame.size.height == 12 {
//                    toolbar.frame.size.height += 32
//                    toolbar.frame.origin.y -= 32
//                }
//                
//                for item in toolbar.items! {
//                    if let view: UIView = item.customView {
//                        view.backgroundColor = UIColor.clearColor()
//                    }
//                }
//            }
//            else {
//                if toolbar.frame.size.height == 44 {
//                    toolbar.frame.size.height -= 32
//                    toolbar.frame.origin.y += 32
//                    for item in toolbar.items! {
//                        if let button: UIButton = item.customView as? UIButton {
//                            button.hidden = true
//                        }
//                        else if let view: UIView = item.customView {
//                            view.backgroundColor = UIColor ( red: 0.6196, green: 0.6392, blue: 0.6902, alpha: 1.0 )
//                        }
//                        else {
//                            item.tintColor = UIColor.clearColor()
//                        }
//                    }
//                }
//            }
//        }) { (isFinish) -> Void in
//                if isUp == true {
//                    for item in toolbar.items! {
//                        if let button: UIButton = item.customView as? UIButton {
//                            button.hidden = false
//                        }
//                        else {
//                            item.tintColor = self.defaultColor
//                        }
//                    }
//                }
//        }
//        
//    }
//    
//    func updateToolBar() {
//        for toolbar in self.toolbarArray {
//            if toolbar.frame.size.height == 12 {
//                for item in toolbar.items! {
//                    if let button: UIButton = item.customView as? UIButton {
//                        button.hidden = true
//                    }
//                    else if let view: UIView = item.customView {
//                        view.backgroundColor = UIColor ( red: 0.6196, green: 0.6392, blue: 0.6902, alpha: 1.0 )
//                    }
//                    else {
//                        item.tintColor = UIColor.clearColor()
//                    }
//                }
//            }
//            else if toolbar.frame.size.height == 44 {
//                for item in toolbar.items! {
//                    if let button: UIButton = item.customView as? UIButton {
//                        button.hidden = false
//                    }
//                    else if let view: UIView = item.customView {
//                        view.backgroundColor = UIColor.clearColor()
//                    }
//                    else {
//                        item.tintColor = self.defaultColor
//                    }
//                }
//            }
//        }
//    }
//    
//    func swipeRecognizer(recognizer: UISwipeGestureRecognizer) {
//        if let toolbar: UIToolbar = recognizer.view as? UIToolbar {
//            if recognizer.direction == .Down {
//                self.showAndHideToolBar(toolbar, isUp: false)
//            }
//            else if recognizer.direction == .Up {
//                self.showAndHideToolBar(toolbar, isUp: true)
//            }
//        }
//    }
//    
//
//     MARK: - NSNotification
//    func keyboardChangeFrame(notification: NSNotification) {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(Double(NSEC_PER_SEC) * 0.1)), dispatch_get_main_queue(), { () -> Void in
//            self.updateToolBar()
//        })
//    }
    
    func keyboardStatus(notification: NSNotification) {
        guard let keyboardValue: NSValue = notification.userInfo?["UIKeyboardFrameEndUserInfoKey"] as? NSValue else {
            return
        }
        
        let keyboardbounds: CGRect = keyboardValue.CGRectValue()
        var keyboardStatus: KeyboardStatus = .WillHide
        switch notification.name {
        case UIKeyboardWillShowNotification:
            keyboardStatus = .WillShow
            break
        case UIKeyboardDidShowNotification:
            keyboardStatus = .DidShow
            break
        case UIKeyboardWillHideNotification:
            keyboardStatus = .WillHide
            break
        case UIKeyboardDidHideNotification:
            keyboardStatus = .DidHide
            break
        default :
            break
        }
        
        self.delegate?.keyboardChangeStatus(notification, status: keyboardStatus, size: keyboardbounds.size)
    }
    
    
    // MARK: - Action
    func prevButtonPush(sender: UIButton) {
        if let item: UITextField = self.textItemArray[sender.tag - 1] as? UITextField {
            item.becomeFirstResponder()
        }
        else if let item: UITextView = self.textItemArray[sender.tag - 1] as? UITextView {
            item.becomeFirstResponder()
        }
    }
    
    func nextButtonPush(sender: UIButton) {
        if let item: UITextField = self.textItemArray[sender.tag + 1] as? UITextField {
            item.becomeFirstResponder()
        }
        else if let item: UITextView = self.textItemArray[sender.tag + 1] as? UITextView {
            item.becomeFirstResponder()
        }
    }
    
    func doneButtonPush(sender: UIBarButtonItem) {
        self.targetView.endEditing(true)
    }
}