//
//  DevgImagePicker.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 3. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//
//
//  # DevgImagePicker
//
//  ## 특징
//  기존 UIImagePickerController대신 최신 프레임워크인 Photos를 이용
//  기본 사진뿐 아니라 클라우드, 사용자 폴더, 고속연사촬영, 스크린샷등 폴더 형태로 재공
//  기존 델리게이트의 복잡함을 빼고 Bolck 으로 간단히 선택 & 촬영후 UIImage 와 NSData로 받을 수 있다
//  아이폰 모든 해상도 대응
//  앨범중에 동영상이나, 타임렙스, 슬로모션은 자동으로 숨길까? 사진으로 취급할까? -- 미정(우선은 사진으로 취급)
//
//  ## 요구사항 및 지원사항
//  iOS 8.0 이상, iPhone 4s 이상
//  DeVice Orientation - Portrait만 지원 -- 나중에??
//
//  ### 주저리
//  촬영 화면도 custom으로 만들까하다가 이미지 갯수도 많아지게 되고 그러면 리소스 형태로 넣을까 하다가.. 소스 파일도 ImagePicker와 나눠야 할것같고 가로 세로 지원이 필수가 될것같고... -- 일단은 보류
//
//  ## 한글 앨범명 사용법
//  1. 적용하고자 하는 프로젝트내 info.Plist 클릭
//  2. Custom iOS Target Properties 에 Key 'Localizations' 추가
//  3. item0: English(Default로 추가되어 있음), item1: Korean 등록
//  4. Enjoy!
//
//  ## 커스텀 이미지 사용법
//  1. 적용하고자 하는 이미지를 프로젝트내 Assets.xcassets에 추가
//  2. PhotoPicker.sharedInstance.show() 할때 topImageString에 이미지 이름 String으로 넣는다
//  3. Enjoy!
//
//  ## StatusBar Color 변경적용하기
//  1. 적용하고자 하는 프로젝트내 info.Plist 클릭
//  2. Custom iOS Targer Properties에 Key 'View controller-based status bar appearance' 추가
//  3. Boolean = NO 로 설정
//  4. Enjoy!
//
//  ## Error Code설명
//  1. 901 사진첩 권한 없음
//  2. 902 카메라 권한 없음
//  3. 904 사진 불러오기 실패

import UIKit
import Devg
import Photos
import AssetsLibrary

typealias ImageSelectBlock = (image: UIImage, imageData: NSData) -> Void
typealias ImageErrorBlock = (errorCode: Int, errorString: String) -> Void

extension NSIndexSet {
    func applIndexPathsFromIndexesWithSection(section: Int) -> [NSIndexPath] {
        var indexPaths: [NSIndexPath] = Array<NSIndexPath>()
        self.enumerateIndexesUsingBlock { (index, stop) -> Void in
            indexPaths.append(NSIndexPath(forItem: index, inSection: section))
        }
        return indexPaths
    }
}

public class DevgImagePicker {
    var statusBarStyle: UIStatusBarStyle = .LightContent
    var viewControllers: [UIViewController] = Array<UIViewController>()
    
    private var _initPicker: DevgImagePickerViewController!
    private var initPicker: DevgImagePickerViewController! {
        get {
            if self._initPicker == nil {
                self._initPicker = self.viewControllers[0] as! DevgImagePickerViewController
            }
            return self._initPicker
        }
    }
    
    private var _initDetailPicker: DevgImageDetailViewController!
    private var initDetailPicker: DevgImageDetailViewController! {
        get {
            if self._initDetailPicker == nil {
                self._initDetailPicker = self.viewControllers[1] as! DevgImageDetailViewController
            }
            return self._initDetailPicker
        }
    }
    
    private static var _sharedInstance: DevgImagePicker!
    public static var sharedInstance: DevgImagePicker {
        get {
            if self._sharedInstance == nil {
                self._sharedInstance = DevgImagePicker()
                self._sharedInstance.viewControllers = DevgImagePicker.imagePickerFormNib()
            }
            return self._sharedInstance
        }
    }
    
    private class func imagePickerFormNib() -> [UIViewController] {
        let nibs = UINib(nibName: "DevgImagePicker", bundle: NSBundle(identifier: "net.devg.DevgImagePicker")).instantiateWithOwner(nil, options: nil)
        return nibs as! [UIViewController]
    }
    
    public func show(viewController: UIViewController, topImageNamed: String? = nil, textColor: UIColor = UIColor.whiteColor(), statusBarStyle: UIStatusBarStyle = .LightContent , selectImage: (image: UIImage, imageData: NSData) -> Void, errorImage: (errorCode: Int, errorString: String) -> Void) {
        self.statusBarStyle = statusBarStyle
        self.initPicker.show(viewController, topImageNamed: topImageNamed, textColor: textColor, statusBarStyle: statusBarStyle, selectImage: selectImage, errorImage: errorImage)
    }
}

class DevgImagePickerViewController: UIViewController, PHPhotoLibraryChangeObserver, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIViewControllerPreviewingDelegate{
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    
    
    var navi: UINavigationController!
    var flowLayout: UICollectionViewFlowLayout!
    var textColors: UIColor!
    var photoList: PHFetchResult!
    var albumList: [PHFetchResult] = Array<PHFetchResult>()
    var photoCollection: PHAssetCollection = PHAssetCollection()
    var gridThumbnailSize: CGSize!
    //    TODO: - 임시
    //    var imageManager: PHCachingImageManager = PHCachingImageManager()
    var imageManager: PHImageManager = PHImageManager.defaultManager()
    
    var selectImageBlock: ImageSelectBlock!
    var errorImageBlock: ImageErrorBlock!
    
    var imagePicker: UIImagePickerController = UIImagePickerController()
    var oldStatusBarStyle: UIStatusBarStyle!
    var newStatusBarStyle: UIStatusBarStyle!

    var preView: UIViewControllerPreviewing? = nil
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.oldStatusBarStyle = UIApplication.sharedApplication().statusBarStyle
        self.collectionView.registerClass(DevgImagePickerCollectionViewCell.self, forCellWithReuseIdentifier: "CollectionCells")
        self.flowLayout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        self.tableView.registerClass(DevgImagePickerTableViewCell.self, forCellReuseIdentifier: "TableCell")
        
        let allPhotoOptions: PHFetchOptions = PHFetchOptions()
        allPhotoOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        //    allPhotoOptions.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.Image.rawValue)
        
        self.photoList = PHAsset.fetchAssetsWithOptions(allPhotoOptions)
        self.albumList.append(self.photoList)
        
        let smartAlbum = PHAssetCollection.fetchAssetCollectionsWithType(.SmartAlbum, subtype: .AlbumRegular, options: nil)
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        /*
        smartAlbum.enumerateObjectsUsingBlock { (assetCollection, index, stop) -> Void in
        if let assetCollection: PHAssetCollection = assetCollection as? PHAssetCollection {
        let assetsFetchResult: PHFetchResult = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: allPhotoOptions)
        print("sub album title is \(assetCollection.localizedTitle), count is \(assetsFetchResult.count)")
        }
        }
        */
        self.albumList.append(smartAlbum)
        
        let topLevelUserCollections = PHCollectionList.fetchTopLevelUserCollectionsWithOptions(nil)
        //    TODO: - 임시 미디어 타입에 대한 결정을 여기서해야 한다
        /*
        topLevelUserCollections.enumerateObjectsUsingBlock { (assetCollection, index, stop) -> Void in
        if let assetCollection: PHAssetCollection = assetCollection as? PHAssetCollection {
        let assetsFetchResult: PHFetchResult = PHAsset.fetchAssetsInAssetCollection(assetCollection, options: allPhotoOptions)
        print("sub album title is \(assetCollection.localizedTitle), count is \(assetsFetchResult.count)")
        }
        }
        */
        self.albumList.append(topLevelUserCollections)
        
        let scale: CGFloat = UIScreen.mainScreen().scale
        let cellSize: CGSize = self.flowLayout.itemSize
        self.gridThumbnailSize = CGSizeMake(cellSize.width * scale, cellSize.height * scale);
        
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        PHPhotoLibrary.sharedPhotoLibrary().registerChangeObserver(self)
        UIApplication.sharedApplication().setStatusBarStyle(self.newStatusBarStyle, animated: true)
        self.tableView.reloadData()
        self.collectionView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        PHPhotoLibrary.sharedPhotoLibrary().unregisterChangeObserver(self)
        UIApplication.sharedApplication().setStatusBarStyle(self.oldStatusBarStyle, animated: true)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        if #available(iOS 9.0, *) {
            if( traitCollection.forceTouchCapability == .Available){
                if self.preView == nil {
                    self.preView = registerForPreviewingWithDelegate(self, sourceView: self.collectionView)
                }
            }
        }
    }
    
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return DevgImagePicker.sharedInstance.statusBarStyle
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UIViewControllerPreviewingDelegate methods
    func previewingContext(previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        if let indexPath = self.collectionView.indexPathForItemAtPoint(location) {
            if let cell = self.collectionView.cellForItemAtIndexPath(indexPath) {
                let detailView = DevgImagePicker.sharedInstance.initDetailPicker
                
                if let asset: PHAsset = self.photoList[indexPath.item - 1] as? PHAsset {
                    detailView.asset = asset
                    detailView.assetCollection = self.photoCollection
                    detailView.topViewImageView.image = self.topImageView.image
                    detailView.selectImageBlock = self.selectImageBlock
                    detailView.errorImageBlock = self.errorImageBlock
                    detailView.textColor = self.textColors
                    detailView.oldStatusBarStyle = self.oldStatusBarStyle
                    detailView.newStatusBarStyle = self.newStatusBarStyle
                    detailView.preferredContentSize = CGSize(width: 0.0, height: 350)
                    detailView.isPreview = true
                    if #available(iOS 9.0, *) {
                        previewingContext.sourceRect = cell.frame
                    } else {
                        // Fallback on earlier versions
                    }
                    return detailView
                }
                return self
            }
            return self
        } 
        return self
    }
    
    func previewingContext(previewingContext: UIViewControllerPreviewing, commitViewController viewControllerToCommit: UIViewController) {
        if let detailView: DevgImageDetailViewController = viewControllerToCommit as? DevgImageDetailViewController {
            detailView.isPreview = false
            showViewController(detailView, sender: self)
        }
        
    }
    
    // MARK: - Functions
    func show(viewController: UIViewController, topImageNamed: String?, textColor: UIColor, statusBarStyle: UIStatusBarStyle, selectImage: ImageSelectBlock, errorImage: ImageErrorBlock) {
        self.selectImageBlock = selectImage
        self.errorImageBlock = errorImage
        self.textColors = textColor
        self.newStatusBarStyle = statusBarStyle
        if ALAssetsLibrary.authorizationStatus() == .Denied {
            self.errorImageBlock(errorCode: 901, errorString: "사진 접근 권한 없음")
            return
        }
        
        if AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) == .Denied {
            self.errorImageBlock(errorCode: 902, errorString: "카메라 접근 권한 없음")
            return
        }
        
        self.navi = UINavigationController(rootViewController: self)
        self.navi.navigationBar.hidden = true
        self.navi.interactivePopGestureRecognizer?.delegate = nil
        self.topButton.setTitle("모든사진 ▼", forState: .Normal)
        self.topButton.setTitle("모든사진 ▲", forState: .Selected)
        self.flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        self.closeButton.setTitleColor(self.textColors, forState: .Normal)
        self.topButton.setTitleColor(self.textColors, forState: .Normal)
        
        if let topBackgroundImageString: String = topImageNamed {
            self.topImageView.image = UIImage(named: topBackgroundImageString)
            self.topLineView.hidden = true
        } else {
            self.topImageView.image = nil
            self.topLineView.hidden = false
        }
        
        viewController.presentViewController(self.navi, animated: true) { () -> Void in
            self.tableView.hidden = false
            self.tableViewBottomConstraint.constant = self.collectionView.frame.size.height
            self.view.layoutIfNeeded()
        }
        self.collectionView.reloadData()
    }
    
    @IBAction func closeButtonPush(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            self.tableView.hidden = true
            self.topButton.selected = false
            self.tableViewBottomConstraint.constant = 0
            self.photoList = self.albumList[0]
            
        }
    }
    
    @IBAction func topButtonPush(sender: UIButton) {
        if sender.selected == true {
            sender.selected = false
            self.tableViewBottomConstraint.constant = self.collectionView.frame.size.height
        }
        else {
            sender.selected = true
            self.tableViewBottomConstraint.constant = 0
        }
        
        UIView.animateWithDuration(0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    // MARK: - UICollectionViewDelegate
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let assetsFetchResults: PHFetchResult = self.photoList {
            return assetsFetchResults.count + 1
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: DevgImagePickerCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("CollectionCells", forIndexPath: indexPath) as! DevgImagePickerCollectionViewCell
        
        if indexPath.item == 0 {
            if let cameraIcon: UIImage = UIImage(named: "ShotCamera", inBundle: NSBundle(identifier: "net.devg.DevgImagePicker"), compatibleWithTraitCollection: nil) {
                cell.imageView.image = cameraIcon
                cell.imageView.contentMode = .Center
            }
        }
        else {
            if let assetsFetchResults: PHFetchResult = self.photoList {
                if let assset: PHAsset = assetsFetchResults[indexPath.item - 1] as? PHAsset {
                    self.imageManager.requestImageForAsset(assset, targetSize: self.gridThumbnailSize, contentMode: .AspectFill, options: nil, resultHandler: { (image, info) -> Void in
                        cell.imageView.contentMode = .ScaleAspectFill
                        cell.imageView.image = image
                    })
                }
            }
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((self.view.frame.size.width / 3) - 2, (self.view.frame.size.width / 3) - 2)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return (self.view.frame.size.width - (((self.view.frame.size.width / 3) - 2) * 3)) / 2
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return (self.view.frame.size.width - (((self.view.frame.size.width / 3) - 2) * 3)) / 2
    }
    
    class DevgImagePickerCollectionViewCell: UICollectionViewCell {
        var imageView: UIImageView!
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            self.contentView.backgroundColor = UIColor.lightGrayColor()
            
            self.imageView = UIImageView(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
            self.imageView.backgroundColor = UIColor.clearColor()
            self.imageView.contentMode = .ScaleAspectFill
            self.imageView.clipsToBounds = true
            self.contentView.addSubview(imageView)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if indexPath.item == 0 {
            self.imagePicker.delegate = self
            self.imagePicker.sourceType = .Camera
            self.presentViewController(self.imagePicker, animated: true, completion: nil)
        }
        else {
            if let detailView: DevgImageDetailViewController = DevgImagePicker.sharedInstance.initDetailPicker {
                if let asset: PHAsset = self.photoList[indexPath.item - 1] as? PHAsset {
                    detailView.asset = asset
                    detailView.assetCollection = self.photoCollection
                    detailView.topViewImageView.image = self.topImageView.image
                    detailView.selectImageBlock = self.selectImageBlock
                    detailView.errorImageBlock = self.errorImageBlock
                    detailView.textColor = self.textColors
                    detailView.oldStatusBarStyle = self.oldStatusBarStyle
                    detailView.newStatusBarStyle = self.newStatusBarStyle
                    detailView.isPreview = false
                    self.navigationController?.pushViewController(detailView, animated: true)
                }
            }
        }
    }
    
    
    // MARK: - UITableViewDelegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.albumList.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numberOfRows: Int = 0
        if section == 0 {
            numberOfRows = 1
        }
        else  {
            let fetchResult: PHFetchResult = self.albumList[section]
            numberOfRows = fetchResult.count
        }
        return numberOfRows
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: DevgImagePickerTableViewCell = tableView.dequeueReusableCellWithIdentifier("TableCell") as! DevgImagePickerTableViewCell
        
        let fetchResult: PHFetchResult = self.albumList[indexPath.section]
        if indexPath.section == 0 {
            cell.titleLabel.text = "모든사진"
            cell.countLabel.text = String(fetchResult.count)
        }
        else {
            if let assetCollection: PHAssetCollection = fetchResult[indexPath.row] as? PHAssetCollection {
                cell.countLabel.text = String(PHAsset.fetchAssetsInAssetCollection(assetCollection, options: nil).count)
            }
            
            if let collection: PHCollection = fetchResult[indexPath.row] as? PHCollection {
                cell.titleLabel.text = collection.localizedTitle!
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.photoList = self.albumList[indexPath.section]
        if indexPath.section == 0 {
            self.topButton.setTitle("모든사진 ▼", forState: .Normal)
            self.topButton.setTitle("모든사진 ▲", forState: .Selected)
        }
        else {
            if let collection: PHAssetCollection = self.photoList[indexPath.row] as? PHAssetCollection {
                
                self.photoCollection = collection
                self.photoList = PHAsset.fetchAssetsInAssetCollection(collection, options: nil)
                self.topButton.setTitle("\(collection.localizedTitle!) ▼", forState: .Normal)
                self.topButton.setTitle("\(collection.localizedTitle!) ▲", forState: .Selected)
            }
        }
        self.collectionView.reloadData()
        self.topButtonPush(self.topButton)
    }
    
    class DevgImagePickerTableViewCell: UITableViewCell {
        var imageViews: UIImageView!
        var titleLabel: UILabel!
        var countLabel: UILabel!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            self.contentView.backgroundColor = UIColor.whiteColor()
            self.imageViews = UIImageView(frame: CGRectMake(5, 5, 50, 50))
            self.imageViews.contentMode = .ScaleAspectFill
            self.imageViews.clipsToBounds = true
            self.contentView.addSubview(self.imageViews)
            
            self.titleLabel = UILabel(frame: CGRectMake(15, 5, 250, 25))
            self.titleLabel.font = UIFont.systemFontOfSize(14)
            self.contentView.addSubview(self.titleLabel)
            
            self.countLabel = UILabel(frame: CGRectMake(15, 30, 250, 25))
            self.countLabel.font = UIFont.systemFontOfSize(13)
            self.countLabel.textColor = UIColor.lightGrayColor()
            self.contentView.addSubview(self.countLabel)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
    }
    
    // MARK: - PHPhotoLibraryChangeObserver
    func photoLibraryDidChange(changeInstance: PHChange) {
        guard let assetsFetchResults: PHFetchResult = self.photoList else {
            return
        }
        
        guard let collectionChanges: PHFetchResultChangeDetails = changeInstance.changeDetailsForFetchResult(assetsFetchResults) else {
            return
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.photoList = collectionChanges.fetchResultAfterChanges
            
            if collectionChanges.hasIncrementalChanges == false  || collectionChanges.hasMoves == true {
                self.collectionView.reloadData()
            }
            else {
                self.collectionView.performBatchUpdates({ () -> Void in
                    if let removedIndexes: NSIndexSet = collectionChanges.removedIndexes {
                        self.collectionView.deleteItemsAtIndexPaths(removedIndexes.applIndexPathsFromIndexesWithSection(0))
                    }
                    
                    if let insertedIndexes: NSIndexSet = collectionChanges.insertedIndexes {
                        self.collectionView.insertItemsAtIndexPaths(insertedIndexes.applIndexPathsFromIndexesWithSection(0))
                    }
                    
                    if let changedIndexes: NSIndexSet = collectionChanges.changedIndexes {
                        self.collectionView.reloadItemsAtIndexPaths(changedIndexes.applIndexPathsFromIndexesWithSection(0))
                    }
                    }, completion:nil)
            }
            
            
            var updateAlbumList: [PHFetchResult] = self.albumList
            var isReload: Bool = false
            
            for (index, collectionFetchResult) in self.albumList.enumerate() {
                if let changeDetails: PHFetchResultChangeDetails = changeInstance.changeDetailsForFetchResult(collectionFetchResult) {
                    updateAlbumList[index] = changeDetails.fetchResultAfterChanges
                    isReload = true
                }
                
            }
            if isReload == true {
                self.albumList = updateAlbumList
                self.tableView.reloadData()
            }
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        self.imagePicker.dismissViewControllerAnimated(true) { () -> Void in
            PHPhotoLibrary.sharedPhotoLibrary().performChanges({ () -> Void in
                PHAssetChangeRequest.creationRequestForAssetFromImage(image)
                }, completionHandler: { (success, rrror) -> Void in
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        if let imageData: NSData = UIImageJPEGRepresentation(image, 1.0) {
                            self.selectImageBlock(image: image, imageData: imageData)
                        }
                        else {
                            self.errorImageBlock(errorCode: 904, errorString: "이미지 불러오기 실패")
                        }
                    })
            })
        }
        
    }
    
}


class DevgImageDetailViewController: UIViewController {
    @IBOutlet weak var topLineView: UIView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var detailImageView: UIImageView!
    @IBOutlet weak var topViewImageView: UIImageView!
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationViewHeightConstraint: NSLayoutConstraint!
    var isPreview: Bool = false
    var isStatusBar: Bool = true {
        didSet {
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    var asset: PHAsset!
    var textColor: UIColor!
    var selectImage: UIImage? = nil
    var errorImageBlock: ImageErrorBlock!
    var assetCollection: PHAssetCollection!
    var selectImageBlock: ImageSelectBlock!
    var oldStatusBarStyle: UIStatusBarStyle!
    var newStatusBarStyle: UIStatusBarStyle!
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(DevgImageDetailViewController.imageTapped(_:)))
        self.detailImageView.userInteractionEnabled = true
        self.detailImageView.addGestureRecognizer(tapGestureRecognizer)
        self.navigationController?.hidesBarsOnTap = true
        
        if self.topViewImageView.image == nil {
            self.topLineView.hidden = false
        }
        else {
            self.topLineView.hidden = true
        }
    }
    
    
    override func viewWillAppear(animated: Bool) {
        UIApplication.sharedApplication().setStatusBarStyle(self.newStatusBarStyle, animated: true)
        self.closeButton.setTitleColor(self.textColor, forState: .Normal)
        self.sendButton.setTitleColor(self.textColor, forState: .Normal)
        
        let options: PHImageRequestOptions = PHImageRequestOptions()
        options.deliveryMode = .HighQualityFormat
        options.networkAccessAllowed = true
        options.progressHandler = { (progress, error, stop, info) -> Void in
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.progressView.progress = Float(progress)
            })
        }
        PHImageManager.defaultManager().requestImageForAsset(self.asset, targetSize: self.targetSize(), contentMode: .AspectFit, options: options) { (resultImage, info) -> Void in
            self.progressView.hidden = true
            guard let image: UIImage = resultImage else {
                return
            }
            self.selectImage = image
            self.detailImageView.image = image
        }
        if self.isPreview == false {
            self.navigationViewHeightConstraint.constant = 64
            self.view.layoutIfNeeded()
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        UIApplication.sharedApplication().setStatusBarStyle(self.oldStatusBarStyle, animated: true)
        self.navigationViewHeightConstraint.constant = 0
        self.view.layoutIfNeeded()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return DevgImagePicker.sharedInstance.statusBarStyle
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return !self.isStatusBar
    }
    
    @available(iOS 9.0, *)
    override func previewActionItems() -> [UIPreviewActionItem] {
        let likeAction = UIPreviewAction(title: "선택", style: .Default) { (action, viewController) -> Void in
            if let image = self.selectImage {
                if let imageData: NSData = UIImageJPEGRepresentation(image, 1.0) {
                    DevgImagePicker.sharedInstance.initPicker.dismissViewControllerAnimated(true, completion: {
                        self.selectImageBlock(image: image, imageData: imageData)
                        self.selectImage = nil
                    })
                }
                else {
                    self.errorImageBlock(errorCode: 904, errorString: "이미지 불러오기 실패")
                }
            }
            else {
                self.errorImageBlock(errorCode: 904, errorString: "이미지 불러오기 실패")
            }
        }
        
        let deleteAction = UIPreviewAction(title: "취소", style: .Default) { (action, viewController) -> Void in
            
        }
        return [likeAction, deleteAction]
    }
    
    // MARK: - Actions
    @IBAction func backButtonPush(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func selectButtonPush(sender: AnyObject) {
        self.dismissViewControllerAnimated(true) { () -> Void in
            if let image = self.selectImage {
                if let imageData: NSData = UIImageJPEGRepresentation(image, 1.0) {
                    self.selectImageBlock(image: image, imageData: imageData)
                    self.selectImage = nil
                }
                else {
                    self.errorImageBlock(errorCode: 904, errorString: "이미지 불러오기 실패")
                }
            }
            else {
                self.errorImageBlock(errorCode: 904, errorString: "이미지 불러오기 실패")
            }
        }
    }
    
    // MARK: - Function
    func imageTapped(image: AnyObject)
    {
        if self.topViewTopConstraint.constant == 0 {
            self.topViewTopConstraint.constant = -64
            
            UIApplication.sharedApplication().setStatusBarHidden(true, withAnimation: .Slide)
            self.isStatusBar = false
            
        }
        else {
            self.topViewTopConstraint.constant = 0
            
            UIApplication.sharedApplication().setStatusBarHidden(false, withAnimation: .Slide)
            self.isStatusBar = true
        }
        
        UIView.animateWithDuration(0.2) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    func targetSize() -> CGSize {
        let scale: CGFloat =  UIScreen.mainScreen().scale
        let targetSize: CGSize = CGSizeMake(CGRectGetWidth(self.detailImageView.bounds) * scale, CGRectGetHeight(self.detailImageView.bounds) * scale)
        return targetSize
    }
}