//
//  DevgExtensionUIImageView.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import UIKit

class DevgImageLoaderManager: NSObject {
    static var sharedInstance: DevgImageLoaderManager = DevgImageLoaderManager()
    var imageLoaders: [UIImageView: DevgImageLoader] = Dictionary<UIImageView, DevgImageLoader>()
    
    func getImageFromUrl(urlString: String, imageView: UIImageView, defaultImage: UIImage?, completion: ((image: UIImage, isCache: Bool) -> Void)?, error: ((error: NSError?) -> Void)? = nil)  {
        if let imageLoader: DevgImageLoader = self.imageLoaders[imageView] {
            imageLoader.connection?.cancel()
            self.imageLoaders[imageView] = nil
        }
        
        let imageLoader: DevgImageLoader = DevgImageLoader()
        imageLoader.getImageFromUrl(urlString, imageView: imageView, defaultImage: defaultImage, completion: completion, error: error)
        
        self.imageLoaders[imageView] = imageLoader
    }
}

class DevgImageLoader: NSObject,NSURLConnectionDelegate, NSURLConnectionDataDelegate {
    var urlString = ""
    var imageData: NSMutableData = NSMutableData()
    var imageView: UIImageView!
    var completion: ((image: UIImage, isCache: Bool) -> Void)?
    var error: ((error: NSError?) -> Void)?
    var connection: NSURLConnection?
    
    func getImageFromUrl(urlString: String, imageView: UIImageView, defaultImage: UIImage?, completion: ((image: UIImage, isCache: Bool) -> Void)?, error: ((error: NSError?) -> Void)? = nil)  {
        self.imageView = imageView
        
        if self.urlString != urlString {
            self.urlString = urlString
            self.completion = completion
            self.error = error
            
            if let string: String = urlString.toURLEncode(.Full) {
                let fileName = string.toBase64()
                
                if Devg.existFile(fileName, type: .Cache) {
                    self.imageView.image = UIImage(contentsOfFile: Devg.filePath(fileName, type: .Cache))
                    if self.imageView.image != nil && self.completion != nil {
                        self.completion!(image: self.imageView.image!, isCache: true)
                        DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
                    }
                }
                else {
                    self.imageData = NSMutableData()
                    if let url = NSURL(string: urlString) {
                        let request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
                        request.addValue("image/*", forHTTPHeaderField: "Accept")
                        self.connection = NSURLConnection(request: request, delegate: self)!
                        self.connection?.start()
                    }
                    else {
                        let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": urlString])
                        self.error?(error: error)
                        DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
                    }
                }
            }
            else {
                let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": urlString])
                self.error?(error: error)
                DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
            }
        }
    }
    
    func connection(_connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
    }
    
    func connection(_connection:NSURLConnection, didFailWithError error:NSError) {
        self.error?(error: error)
        DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
    }
    
    func connection(connection : NSURLConnection, didReceiveData data:NSData) {
        self.imageData.appendData(data)
    }
    
    func connectionDidFinishLoading(connection : NSURLConnection) {
        if let image = UIImage(data: self.imageData) {
            if let string: String = urlString.toURLEncode(.Full) {
                let fileName: String = string.toBase64()
                
                self.imageData.writeToFile(Devg.filePath(fileName, type: .Cache), atomically: true)
                
                self.imageView.image = image
                
                if self.imageView.image != nil && self.completion != nil {
                    self.completion!(image: self.imageView.image!, isCache: false)
                    DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
                }
                else if self.error != nil && self.imageView.image == nil {
                    self.error!(error: nil)
                    DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
                }
            }
            else {
                self.error!(error: nil)
                DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
            }
        }
        else if self.error != nil {
            self.error!(error: nil)
            DevgImageLoaderManager.sharedInstance.imageLoaders[self.imageView] = nil
        }
    }
}

public extension UIImageView {
    public func setImageFromUrl(urlString: String, defaultImage: UIImage? = nil, completion: ((image: UIImage, isCache: Bool) -> Void)? = nil, error: ((error: NSError?) -> Void)? = nil) {
        if let image: UIImage = defaultImage {
            self.image = image
        }
        DevgImageLoaderManager.sharedInstance.getImageFromUrl(urlString, imageView: self, defaultImage: defaultImage, completion: completion, error: error)
    }
    
}
