//
//  Devg.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public enum PathType {
    case Document
    case Cache
}

public class Devg: NSObject {
    // MARK: - System
    
    /// iOS 기기 정보.
    ///
    /// :returns: 기기 정보.
    public class func device() -> String {
        let deviceList =   ["i386":         "Simulator",
                            "x86_64":       "Simulator",
                            "iPod1,1":      "iPod Touch",       // (Original)
            "iPod2,1":      "iPod Touch 2",     // (Second Generation)
            "iPod3,1":      "iPod Touch 3",     // (Third Generation)
            "iPod4,1":      "iPod Touch 4",     // (Fourth Generation)
            "iPhone1,1":    "iPhone 1",         // (Original)
            "iPhone1,2":    "iPhone 3G",        // (3G)
            "iPhone2,1":    "iPhone 3GS",       // (3GS)
            "iPad1,1":      "iPad 1",           // (Original)
            "iPad2,1":      "iPad 2",           //
            "iPad3,1":      "iPad 3",           // (3rd Generation)
            "iPhone3,1":    "iPhone 4",         //
            "iPhone4,1":    "iPhone 4S",        //
            "iPhone5,1":    "iPhone 5",         // (model A1428, AT&T/Canada)
            "iPhone5,2":    "iPhone 5",         // (model A1429, everything else)
            "iPad3,4":      "iPad 4",           // (4th Generation)
            "iPad2,5":      "iPad Mini 1",      // (Original)
            "iPhone5,3":    "iPhone 5c",        // (model A1456, A1532 | GSM)
            "iPhone5,4":    "iPhone 5c",        // (model A1507, A1516, A1526 (China), A1529 | Global)
            "iPhone6,1":    "iPhone 5s",        // (model A1433, A1533 | GSM)
            "iPhone6,2":    "iPhone 5s",        // (model A1457, A1518, A1528 (China), A1530 | Global)
            "iPad4,1":      "iPad Air 1",       // 5th Generation iPad (iPad Air) - Wifi
            "iPad4,2":      "iPad Air 2",       // 5th Generation iPad (iPad Air) - Cellular
            "iPad4,4":      "iPad Mini 2",      // (2nd Generation iPad Mini - Wifi)
            "iPad4,5":      "iPad Mini 2",      // (2nd Generation iPad Mini - Cellular)
            "iPhone7,1":    "iPhone 6 Plus",    // All iPhone 6 Plus's
            "iPhone7,2":    "iPhone 6",         // All iPhone 6's
            "iPhone8,1":    "iPhone 6s",
            "iPhone8,2":    "iPhone 6s Plus",
            "iPad6,7":      "iPad Pro",
            "iPad6,8":      "iPad Pro"
        ]
        
        var systemInfo: utsname = utsname(sysname: (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                                          nodename: (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                                          release: (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                                          version: (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
                                          machine: (0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
        )
        uname(&systemInfo)
        
        let machine = systemInfo.machine
        var identifier = ""
        let mirror = _reflect(machine)
        for i in 0..<_reflect(machine).count {
            if mirror[i].1.value as! Int8 == 0 {
                break
            }
            identifier.append(UnicodeScalar(UInt8(mirror[i].1.value as! Int8)))
        }
        
        return deviceList[identifier] ?? identifier
    }
    
    /// App Bundle Identifier
    ///
    /// :returns: String
    public class func bundelID() -> String {
        return Devg.defaultPlistValueForKey("CFBundleIdentifier") as! String
    }
    
    /// 앱 버전 정보.
    ///
    /// :returns: Float
    public class func appVersion() -> String {
        return Devg.defaultPlistValueForKey("CFBundleShortVersionString") as! String
    }
    
    public class func buildVersion() -> String {
        return Devg.defaultPlistValueForKey("CFBundleVersion") as! String
    }
    
    /// 기본 Plist 파일에서 정보 가져 오기.
    ///
    /// :param: key Plist에 기록된 키.
    /// :returns: 해당 키에 저장된 값.
    public class func defaultPlistValueForKey(key: String) -> AnyObject? {
        return NSBundle.mainBundle().objectForInfoDictionaryKey(key)
    }
    
    /// NSData로 전달된 기기 토큰을 String으로 변환.
    ///
    /// :param: deviceToken appDelegate에서 전달 받은 기기 토큰 정보.
    /// :retruns: String으로 변환된 토큰.
    public class func tokenString(deviceToken: NSData) -> String {
        let tokenString: String = deviceToken.description.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "<>"))
        return tokenString.stringByReplacingOccurrencesOfString(" ", withString: "", range: nil)
    }
    
    
    // MARK: - File
    
    /// OS에서 앱에게 활당해준 경로값을 찾음.
    ///
    /// :param: name 사용자가 사용할 파일 이름.
    /// :param: type OS에서 앱에 활당해준 용도.
    /// :returns: 사용자가 사용할 파일의 실질적인 경로.
    public class func filePath(name: String, type: PathType) -> String {
        var searchPath: NSSearchPathDirectory
        switch type {
        case .Document:
            searchPath = .DocumentDirectory
            break
        case .Cache:
            searchPath = .CachesDirectory
        }
        let savePath: NSString = NSSearchPathForDirectoriesInDomains(searchPath, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
        return savePath.stringByAppendingPathComponent(name)
    }
    
    /// 파일 존재 여부 확인.
    ///
    /// :param: name 확인할 사용할 파일 이름.
    /// :param: type 해당 파일을 찾을 폴더의 종류.
    /// :returns: 존재 여부.
    public class func existFile(name: String, type: PathType = .Document) -> Bool {
        return NSFileManager.defaultManager().fileExistsAtPath(Devg.filePath(name, type: type))
    }
    
    /// 파일 삭제.
    ///
    /// :param: name 삭제할 파일 이름.
    /// :param: type 해당 파일을 삭제할 폴더의 종류.
    /// :returns: 삭제 성공 여부.
    public class func deleteFile(name: String, type: PathType = .Document) -> Bool {
        if Devg.existFile(name, type: type) {
            do {
                try NSFileManager.defaultManager().removeItemAtPath(Devg.filePath(name, type: type))
                return true
            }
            catch {
                return false
            }
        }
        else {
            return true
        }
    }
    
    /// 파일 생성.
    ///
    /// :param: name 생성할 파일 이름.
    /// :param: contents 생성할 파일 내용.
    /// :param: type 해당 파일을 생성할 폴더의 종류.
    /// :returns: 생성 성공 여부.
    public class func createFile(name: String, contents: String, type: PathType = .Document) -> Bool {
        Devg.deleteFile(name, type: type)
        return NSFileManager.defaultManager().createFileAtPath(Devg.filePath(name, type: type), contents: contents.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true), attributes: nil)
    }
    
    /// 파일 내용 가져오기.
    ///
    /// :param: name 내용을 가져 올 파일 이름.
    /// :param: type 해당 파일을 가져 올 폴더의 종류.
    /// :returns: 파일 내용.
    public class func getContents(name: String, type: PathType = .Document) -> String? {
        do {
            let string: String = try String(contentsOfFile: Devg.filePath(name, type: type), encoding: NSUTF8StringEncoding)
            return string
        }
        catch {
            return nil
        }
    }
    
    /// 앱에 삽입된 파일 내용 가져오기.
    ///
    /// :param: name 내용을 가져 올 파일 이름.
    /// :param: extention 해당 파일을 가져 올 폴더의 종류.
    /// :returns: 파일 내용.
    public class func getContentsFromApp(name: String, extention: String) -> String? {
        if let path: String = NSBundle.mainBundle().pathForResource(name, ofType: extention) {
            do {
                let string: String = try String(contentsOfFile: path, encoding: NSUTF8StringEncoding)
                return string
            }
            catch {
                return nil
            }
        }
        else {
            return nil
        }
    }
    
    // MARK: - Store
    
    /// 설정 값을 저장하기.
    ///
    /// :param: key 설정 값을 저장할 키.
    /// :param: value 저장할 설정 값.
    /// :returns: String
    public class func putStore<T>(key: String, value: T) {
        NSUserDefaults.standardUserDefaults().setObject(value as? AnyObject, forKey: key)
    }
    
    /// 설정 값을 가져오기.
    ///
    /// :param: key 설정 값을 가져 올 키.
    /// :return: String
    //    class func getStore(key: String) -> String {
    //        var string: String = ""
    //        if let objectString: String = NSUserDefaults.standardUserDefaults().objectForKey(key) as? String {
    //            string = objectString
    //        }
    //
    //        return string
    //    }
    
    /// 설정 값을 삭제하기.
    public class func deleteStore(key: String) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(key)
    }
    
    public class func getStore<T>(key: String) -> T? {
        var returnObject: T? = nil
        if NSUserDefaults.standardUserDefaults().objectForKey(key) != nil {
            returnObject = NSUserDefaults.standardUserDefaults().objectForKey(key) as? T
        }
        
        return returnObject
    }
    
    // MARK: - Web
    
    /// Curl Command Line을 URL 형태로 변환.
    ///
    /// :param: curl Curl Command Line
    /// :returns: URL
    public class func curlToUrl(curl: String) -> String {
        let curls = curl.stringByReplacingOccurrencesOfString("\"", withString: "'", range: nil).componentsSeparatedByString("http")
        let urls = curls[1].componentsSeparatedByString("' -d '")
        var urlString = "http"
        var i = 0
        for url: String in urls {
            if i > 1 {
                urlString = urlString + "&"
            }
            else if i > 0 {
                urlString = urlString + "?"
            }
            
            urlString = urlString + url
            i += 1
        }
        urlString = urlString.stringByReplacingOccurrencesOfString("'", withString: "", range: nil)
        
        return urlString
    }
    
    /// URL String의 Parameter를 Dictionary로 변환.
    ///
    /// :param: urlString String
    /// :retruns: Dictionary
    public class func urlStringParser(urlString: String) -> [String: String] {
        var parse = [String: String]()
        let urls = urlString.componentsSeparatedByString("?")
        var parameterString: String
        
        if urls.count == 2 {
            parameterString = urls[1]
        }
        else {
            parameterString = urls[0]
        }
        
        let parameters = parameterString.componentsSeparatedByString("&")
        for parameter in parameters {
            let split = parameter.componentsSeparatedByString("=")
            if split.count > 1 {
                var value: String = ""
                for i in 1..<split.count {
                    value = value + split[i]
                }
                parse[split[0]] = value
            }
        }
        
        return parse
    }
    
    /// URL String에 get parameter를 추가.
    ///
    /// :param: params 추가할 parameters Dictionary<String, String>
    /// :returns: params가 get으로 추가된 URL String
    public class func addGetParameters(params: [String: String], toUrlString: String) -> String {
        var newUrlString = toUrlString
        
        if newUrlString.rangeOfString("?") != nil{
            newUrlString = "\(newUrlString)&" + Devg.toGetParameters(params)
        }
        else {
            newUrlString = "\(newUrlString)?" + Devg.toGetParameters(params)
        }
        
        return newUrlString
    }
    
    public class func toGetParameters(dictionary: [String: String]) -> String {
        var newUrlString = ""
        
        for (key, value) in dictionary {
            if newUrlString.characters.count > 0 {
                newUrlString = "\(newUrlString)&\(key)=\(value)"
            }
            else {
                newUrlString = "\(key)=\(value)"
            }
        }
        
        return newUrlString
    }
}
