//
//  DevgLabel.swift
//  IM00Test
//
//  Created by Lee JinWook on 2016. 6. 2..
//  Copyright © 2016년 Zen Red. All rights reserved.
//

import UIKit
import Foundation
import Devg

@objc public protocol DevgLabelDelegate: class {
    optional func clickDetection(label: DevgLabel, text: String, type: String)
}


@IBDesignable public class DevgLabel: UILabel {
    // MARK: - 공통 설정
    @IBInspectable public var scrollEnabled: Bool = false
    @IBInspectable public var scrollSpeed: CGFloat = 10
    @IBInspectable public var scrollFadeLength: CGFloat = 20.0
    
    @IBInspectable public var linkEnabled: Bool = false
    @IBInspectable public var linkTypeMention: Bool = true
    @IBInspectable public var linkTypeTag: Bool = true
    @IBInspectable public var linkTypeURL: Bool = true
    @IBInspectable public var linkTypePhone: Bool = true
    
    @IBInspectable public var mentionColor: UIColor = .blueColor() {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var mentionSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var hashtagColor: UIColor = .blueColor() {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var hashtagSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var URLColor: UIColor = .blueColor() {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var URLSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var phoneColor: UIColor = .blueColor() {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    @IBInspectable public var phoneSelectedColor: UIColor? {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    
    @IBInspectable public var lineSpacing: Float = 0 {
        didSet {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    
    
    public override class func layerClass() -> AnyClass {
        return CAReplicatorLayer.self
    }
    
    public override var text: String? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.text
            }
            else {
                return super.text
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.text == newValue {
                    return
                }
                self.subLabel.text = newValue
                self.updateLabel()
                super.text = newValue
            }
            else if self.linkEnabled == true {
                super.text = newValue
                self.updateTextStorage()
            }
        }
    }
    
    public override var textColor: UIColor! {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.textColor
            }
            else {
                return super.textColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.textColor = newValue
                self.updateLabel()
                super.textColor = newValue
            }
            else if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    
    public override var backgroundColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.backgroundColor
            }
            else {
                return super.backgroundColor
            }
        }
        set {
            super.backgroundColor = newValue
            if self.scrollEnabled == true {
                self.subLabel.backgroundColor = newValue
                self.updateLabel()
            }
        }
    }
    
    public override var shadowColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.shadowColor
            }
            else {
                return super.shadowColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.shadowColor = newValue
            }
            super.shadowColor = newValue
        }
    }
    
    public override var shadowOffset: CGSize {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.shadowOffset
            }
            else {
                return super.shadowOffset
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.shadowOffset = newValue
            }
            super.shadowOffset = newValue
        }
    }
    
    public override var highlightedTextColor: UIColor? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.highlightedTextColor
            }
            else {
                return super.highlightedTextColor
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.highlightedTextColor = newValue
            }
            super.highlightedTextColor = newValue
        }
    }
    
    public override var highlighted: Bool {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.highlighted
            }
            else {
                return super.highlighted
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.highlighted = newValue
            }
            super.highlighted = newValue
        }
    }
    
    public override var enabled: Bool {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.enabled
            }
            else {
                return super.enabled
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.enabled = newValue
            }
            super.enabled = newValue
        }
    }
    
    public override var attributedText: NSAttributedString? {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.attributedText
            }
            else {
                return super.attributedText
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.attributedText == newValue {
                    return
                }
                self.subLabel.attributedText = newValue
                super.attributedText = newValue
            }
            else if self.linkEnabled == true {
                super.attributedText = newValue
                self.updateTextStorage()
            }
        }
    }
    
    public override var font: UIFont! {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.font
            }
            else {
                return super.font
            }
        }
        set {
            if self.scrollEnabled == true {
                if self.subLabel.font == newValue {
                    return
                }
                self.subLabel.font = newValue
                super.font = newValue
            }
            else if self.scrollEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    
    public override var numberOfLines: Int {
        get {
            return super.numberOfLines
        }
        set {
            if self.scrollEnabled == true {
                super.numberOfLines = 1
            }
            else {
                self.textContainer.maximumNumberOfLines = self.numberOfLines
            }
        }
    }
    
    public override var adjustsFontSizeToFitWidth: Bool {
        get {
            return super.adjustsFontSizeToFitWidth
        }
        set {
            if self.scrollEnabled == true {
                super.adjustsFontSizeToFitWidth = false
            }
            else {
                super.adjustsFontSizeToFitWidth = newValue
            }
        }
    }
    
    public override var minimumScaleFactor: CGFloat {
        get {
            return super.minimumScaleFactor
        }
        set {
            if self.scrollEnabled == true {
                super.minimumScaleFactor = 0.0
            }
            else {
                super.minimumScaleFactor = newValue
            }
        }
    }
    
    public override var baselineAdjustment: UIBaselineAdjustment {
        get {
            if self.scrollEnabled == true {
                return self.subLabel.baselineAdjustment
            }
            else {
                return super.baselineAdjustment
            }
        }
        set {
            if self.scrollEnabled == true {
                self.subLabel.baselineAdjustment = newValue
            }
            super.baselineAdjustment = newValue
        }
    }
    
    public override var textAlignment: NSTextAlignment {
        get {
            return super.textAlignment
        }
        set {
            if self.linkEnabled == true {
                self.updateTextStorage(parseText: false)
            }
        }
    }
    
    public override var lineBreakMode: NSLineBreakMode {
        get {
            return super.lineBreakMode
        }
        set {
            if self.linkEnabled == true {
                self.textContainer.lineBreakMode = self.lineBreakMode
            }
        }
    }
    
    
    deinit {
        if self.scrollEnabled == true {
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        if self.scrollEnabled == true && self.linkEnabled == true {
            e("스크롤 라벨과 링크라벨을 동시에 사용할수 없습니다")
        }
        if self.scrollEnabled == true {
            self.setup()
        }
        else if linkEnabled == true {
            self.setupLabel()
        }
    }
    
    public override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesBegan(touches, withEvent: event)
    }
    
    public override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesMoved(touches, withEvent: event)
    }
    
    public override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        guard let touch = touches?.first else {
            return
        }
        self.onTouch(touch)
        super.touchesCancelled(touches, withEvent: event)
    }
    
    public override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        if self.onTouch(touch) {
            return
        }
        super.touchesEnded(touches, withEvent: event)
    }
    
    public override func drawTextInRect(rect: CGRect) {
        let range = NSRange(location: 0, length: self.textStorage.length)
        
        self.textContainer.size = rect.size
        let newOrigin = self.textOrigin(inRect: rect)
        
        self.layoutManager.drawBackgroundForGlyphRange(range, atPoint: newOrigin)
        self.layoutManager.drawGlyphsForGlyphRange(range, atPoint: newOrigin)
    }
    
    public enum LinkType {
        case Mention
        case Tag
        case URL
        case Phone
        case None
    }
    
    enum LinkElement {
        case Mention(String)
        case Tag(String)
        case URL(String)
        case Phone(String)
        case None
    }
    
    typealias FilterPredicate = (String -> Bool)
    
    static let hashtagRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$)#[\\p{L}0-9_]*", options: [.CaseInsensitive])
    static let mentionRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$|[.])@[\\p{L}0-9_]*", options: [.CaseInsensitive]);
    static let urlDetector = try? NSRegularExpression(pattern: "(^|[\\s.:;?\\-\\]<\\(])" + "((https?://|www\\.|pic\\.)[-\\w;/?:@&=+$\\|\\_.!~*\\|'()\\[\\]%#,☺]+[\\w/#](\\(\\))?)" + "(?=$|[\\s',\\|\\(\\).:;?\\-\\[\\]>\\)])", options: [.CaseInsensitive])
    static let phoneRegex = try? NSRegularExpression(pattern: "(?:^|\\s|$|[.])((\\d{2,3})(?:\\-|\\ )?(\\d{3,4})(?:\\-|\\ )?(\\d{4}))", options: [.CaseInsensitive]);
    
    static func getMentions(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let mentionRegex = mentionRegex else { return [] }
        return mentionRegex.matchesInString(text, options: [], range: range)
    }
    
    static func getHashtags(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let hashtagRegex = hashtagRegex else { return [] }
        return hashtagRegex.matchesInString(text, options: [], range: range)
    }
    
    static func getURLs(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let urlDetector = urlDetector else { return [] }
        return urlDetector.matchesInString(text, options: [], range: range)
    }
    
    static func getPhone(fromText text: String, range: NSRange) -> [NSTextCheckingResult] {
        guard let phoneRegex = phoneRegex else { return [] }
        return phoneRegex.matchesInString(text, options: [], range: range)
    }
    
    static func createMentionElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let mentions = DevgLabel.getMentions(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for mention in mentions where mention.range.length > 2 {
            let range = NSRange(location: mention.range.location + 1, length: mention.range.length - 1)
            var word = nsstring.substringWithRange(range)
            if word.hasPrefix("@") {
                word.removeAtIndex(word.startIndex)
            }
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.Mention(word)
                elements.append((mention.range, element))
            }
        }
        return elements
    }
    
    static func createHashtagElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let hashtags = DevgLabel.getHashtags(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for hashtag in hashtags where hashtag.range.length > 2 {
            let range = NSRange(location: hashtag.range.location + 1, length: hashtag.range.length - 1)
            var word = nsstring.substringWithRange(range)
            if word.hasPrefix("#") {
                word.removeAtIndex(word.startIndex)
            }
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.Tag(word)
                elements.append((hashtag.range, element))
            }
        }
        return elements
    }
    
    static func createPhoneElements(fromText text: String, range: NSRange, filterPredicate: FilterPredicate?) -> [(range: NSRange, element: LinkElement)] {
        let phones = DevgLabel.getPhone(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for phone in phones where phone.range.length > 2 {
            let word = nsstring.substringWithRange(phone.range)
            
            if filterPredicate?(word) ?? true {
                let element = LinkElement.Phone(word)
                elements.append((phone.range, element))
            }
        }
        return elements
    }
    
    static func createURLElements(fromText text: String, range: NSRange) -> [(range: NSRange, element: LinkElement)] {
        let urls = DevgLabel.getURLs(fromText: text, range: range)
        let nsstring = text as NSString
        var elements: [(range: NSRange, element: LinkElement)] = []
        
        for url in urls where url.range.length > 2 {
            let word = nsstring.substringWithRange(url.range)
                .stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
            let element = LinkElement.URL(word)
            elements.append((url.range, element))
        }
        return elements
    }
    
    
    public func filterMention(predicate: (String) -> Bool) {
        self.mentionFilterPredicate = predicate
        self.updateTextStorage()
    }
    
    public func filterHashtag(predicate: (String) -> Bool) {
        self.hashtagFilterPredicate = predicate
        self.updateTextStorage()
    }
    
    private var mentionFilterPredicate: ((String) -> Bool)?
    private var hashtagFilterPredicate: ((String) -> Bool)?
    private var phoneFilterPredicate: ((String) -> Bool)?
    private var selectedElement: (range: NSRange, element: LinkElement)?
    private var heightCorrection: CGFloat = 0
    private lazy var textStorage = NSTextStorage()
    private lazy var layoutManager = NSLayoutManager()
    private lazy var textContainer = NSTextContainer()
    internal lazy var activeElements: [LinkType: [(range: NSRange, element: LinkElement)]] = [.Mention: [], .Tag: [], .Phone: [], .URL: []]
    
    private func setupLabel() {
        if userInteractionEnabled == false {
            self.textStorage.addLayoutManager(self.layoutManager)
            self.layoutManager.addTextContainer(self.textContainer)
            self.textContainer.lineFragmentPadding = 0
            self.textContainer.lineBreakMode = self.lineBreakMode
            self.textContainer.maximumNumberOfLines = self.numberOfLines
            userInteractionEnabled = true
        }
        self.updateTextStorage()
    }
    
    private func updateTextStorage(parseText parseText: Bool = true) {
        guard let attributedText = self.attributedText where attributedText.length > 0 else {
            self.clearActiveElements()
            self.textStorage.setAttributedString(NSAttributedString())
            setNeedsDisplay()
            return
        }
        
        let mutAttrString = self.addLineBreak(attributedText)
        
        if parseText {
            self.clearActiveElements()
            self.parseTextAndExtractActiveElements(mutAttrString)
        }
        
        self.addLinkAttribute(mutAttrString)
        self.textStorage.setAttributedString(mutAttrString)
        self.setNeedsDisplay()
    }
    
    private func clearActiveElements() {
        self.selectedElement = nil
        for (type, _) in self.activeElements {
            self.activeElements[type]?.removeAll()
        }
    }
    
    private func textOrigin(inRect rect: CGRect) -> CGPoint {
        let usedRect = self.layoutManager.usedRectForTextContainer(self.textContainer)
        self.heightCorrection = (rect.height - usedRect.height)/2
        let glyphOriginY = self.heightCorrection > 0 ? rect.origin.y + self.heightCorrection : rect.origin.y
        return CGPoint(x: rect.origin.x, y: glyphOriginY)
    }
    
    private func addLinkAttribute(mutAttrString: NSMutableAttributedString) {
        var range = NSRange(location: 0, length: 0)
        var attributes = mutAttrString.attributesAtIndex(0, effectiveRange: &range)
        
        attributes[NSFontAttributeName] = self.font!
        attributes[NSForegroundColorAttributeName] = self.textColor
        mutAttrString.addAttributes(attributes, range: range)
        
        attributes[NSForegroundColorAttributeName] = self.mentionColor
        
        for (type, elements) in self.activeElements {
            switch type {
            case .Mention: attributes[NSForegroundColorAttributeName] = self.mentionColor
            case .Tag: attributes[NSForegroundColorAttributeName] = self.hashtagColor
            case .URL: attributes[NSForegroundColorAttributeName] = self.URLColor
            case .Phone: attributes[NSForegroundColorAttributeName] = self.phoneColor
            case .None: ()
            }
            
            for element in elements {
                mutAttrString.setAttributes(attributes, range: element.range)
            }
        }
    }
    
    private func parseTextAndExtractActiveElements(attrString: NSAttributedString) {
        let textString = attrString.string
        let textLength = textString.utf16.count
        let textRange = NSRange(location: 0, length: textLength)
        
        if self.linkTypeURL == true {
            let urlElements = DevgLabel.createURLElements(fromText: textString, range: textRange)
            self.activeElements[.URL]?.appendContentsOf(urlElements)
        }
        
        if self.linkTypeTag == true {
            let hashtagElements = DevgLabel.createHashtagElements(fromText: textString, range: textRange, filterPredicate: self.hashtagFilterPredicate)
            self.activeElements[.Tag]?.appendContentsOf(hashtagElements)
        }
        
        if self.linkTypePhone == true {
            let phoneElements = DevgLabel.createPhoneElements(fromText: textString, range: textRange, filterPredicate: self.phoneFilterPredicate)
            self.activeElements[.Phone]?.appendContentsOf(phoneElements)
        }
        
        if self.linkTypeMention == true {
            let mentionElements = DevgLabel.createMentionElements(fromText: textString, range: textRange, filterPredicate: self.mentionFilterPredicate)
            self.activeElements[.Mention]?.appendContentsOf(mentionElements)
        }
    }
    
    private func addLineBreak(attrString: NSAttributedString) -> NSMutableAttributedString {
        let mutAttrString = NSMutableAttributedString(attributedString: attrString)
        
        var range = NSRange(location: 0, length: 0)
        var attributes = mutAttrString.attributesAtIndex(0, effectiveRange: &range)
        
        let paragraphStyle = attributes[NSParagraphStyleAttributeName] as? NSMutableParagraphStyle ?? NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.ByWordWrapping
        paragraphStyle.alignment = textAlignment
        paragraphStyle.lineSpacing = CGFloat(lineSpacing)
        
        attributes[NSParagraphStyleAttributeName] = paragraphStyle
        mutAttrString.setAttributes(attributes, range: range)
        
        return mutAttrString
    }
    
    private func updateAttributesWhenSelected(isSelected: Bool) {
        guard let selectedElement = self.selectedElement else {
            return
        }
        
        var attributes = self.textStorage.attributesAtIndex(0, effectiveRange: nil)
        if isSelected {
            switch selectedElement.element {
            case .Mention(_): attributes[NSForegroundColorAttributeName] = self.mentionSelectedColor ?? self.mentionColor
            case .Tag(_): attributes[NSForegroundColorAttributeName] = self.hashtagSelectedColor ?? self.hashtagColor
            case .URL(_): attributes[NSForegroundColorAttributeName] = self.URLSelectedColor ?? self.URLColor
            case .Phone(_): attributes[NSForegroundColorAttributeName] = self.phoneSelectedColor ?? self.phoneColor
            case .None: ()
            }
        } else {
            switch selectedElement.element {
            case .Mention(_): attributes[NSForegroundColorAttributeName] = self.mentionColor
            case .Tag(_): attributes[NSForegroundColorAttributeName] = self.hashtagColor
            case .URL(_): attributes[NSForegroundColorAttributeName] = self.URLColor
            case .Phone(_): attributes[NSForegroundColorAttributeName] = self.phoneColor
            case .None: ()
            }
        }
        
        self.textStorage.addAttributes(attributes, range: selectedElement.range)
        
        setNeedsDisplay()
    }
    
    private func elementAtLocation(location: CGPoint) -> (range: NSRange, element: LinkElement)? {
        guard self.textStorage.length > 0 else {
            return nil
        }
        
        var correctLocation = location
        correctLocation.y = correctLocation.y - self.heightCorrection
        let boundingRect = self.layoutManager.boundingRectForGlyphRange(NSRange(location: 0, length: self.textStorage.length), inTextContainer: self.textContainer)
        guard boundingRect.contains(correctLocation) else {
            return nil
        }
        
        let index = self.layoutManager.glyphIndexForPoint(correctLocation, inTextContainer: self.textContainer)
        
        for element in self.activeElements.map({ $0.1 }).flatten() {
            if index >= element.range.location && index <= element.range.location + element.range.length {
                return element
            }
        }
        
        return nil
    }
    
    func onTouch(touch: UITouch) -> Bool {
        let location = touch.locationInView(self)
        var avoidSuperCall = false
        
        switch touch.phase {
        case .Began, .Moved:
            if let element = self.elementAtLocation(location) {
                if element.range.location != self.selectedElement?.range.location || element.range.length != self.selectedElement?.range.length {
                    self.updateAttributesWhenSelected(false)
                    self.selectedElement = element
                    self.updateAttributesWhenSelected(true)
                }
                avoidSuperCall = true
            } else {
                self.updateAttributesWhenSelected(false)
                self.selectedElement = nil
            }
        case .Ended:
            guard let selectedElement = self.selectedElement else {
                return avoidSuperCall
            }
            
            switch selectedElement.element {
            case .Mention(let userHandle): self.didTapMention(userHandle)
            case .Tag(let hashtag): self.didTapHashtag(hashtag)
            case .URL(let url): self.didTapStringURL(url)
            case .Phone(let phone): self.didTapPhone(phone)
            case .None: ()
            }
            
            let when = dispatch_time(DISPATCH_TIME_NOW, Int64(0.25 * Double(NSEC_PER_SEC)))
            dispatch_after(when, dispatch_get_main_queue()) {
                self.updateAttributesWhenSelected(false)
                self.selectedElement = nil
            }
            avoidSuperCall = true
        case .Cancelled:
            self.updateAttributesWhenSelected(false)
            self.selectedElement = nil
        case .Stationary:
            break
        }
        
        return avoidSuperCall
    }
    
    private func didTapMention(username: String) {
        if let click = self.delegate?.clickDetection {
            click(self, text: username, type: "Mention")
        }
    }
    
    private func didTapHashtag(hashtag: String) {
        if let click = self.delegate?.clickDetection {
            click(self, text: hashtag, type: "Hashtag")
        }
        return
    }
    
    private func didTapPhone(phone: String) {
        if let click = self.delegate?.clickDetection {
            click(self, text: phone, type: "Phone")
        }
    }
    
    private func didTapStringURL(stringURL: String) {
        if let click = self.delegate?.clickDetection {
            click(self, text: stringURL, type: "URL")
        }
    }
    
    // MARK: -  Scroll Label
    
    public weak var delegate: DevgLabelDelegate? = nil
    
    private var subLabel: UILabel = UILabel()
    private var subLabelFrame: CGRect = CGRect.zero
    private var awayOffset: CGFloat = 0.0
    private var animationDuration: CGFloat = 0.0
    private typealias MLAnimationCompletion = (finished: Bool) -> ()
    
    private var maskLayer: CAGradientLayer? {
        return self.layer.mask as! CAGradientLayer?
    }
    
    public enum SpeedLimit {
        case Rate(CGFloat)
        case Duration(CGFloat)
        
        var value: CGFloat {
            switch self {
            case .Rate(let rate):
                return rate
            case .Duration(let duration):
                return duration
            }
        }
    }
    
    private var repliLayer: CAReplicatorLayer {
        return self.layer as! CAReplicatorLayer
    }
    
    public var isPaused: Bool {
        return (self.subLabel.layer.speed == 0.0)
    }
    
    
    
    public var speed: SpeedLimit = .Duration(7.0) {
        didSet {
            switch (self.speed, oldValue) {
            case (.Rate(let a), .Rate(let b)) where a == b:
                return
            case (.Duration(let a), .Duration(let b)) where a == b:
                return
            default:
                self.updateLabel()
            }
        }
    }
    
    private struct Scroller {
        typealias Scroll = (layer: CALayer, anim: CAKeyframeAnimation)
        init(generator gen: (interval: CGFloat, delay: CGFloat) -> [Scroll]) {
            self.generator = gen
        }
        
        let generator: (interval: CGFloat, delay: CGFloat) -> [Scroll]
        var scrolls: [Scroll]? = nil
        
        mutating func generate(interval: CGFloat, delay: CGFloat) -> [Scroll] {
            if let existing = self.scrolls {
                return existing
            }
            else {
                self.scrolls = self.generator(interval: interval, delay: delay)
                return self.scrolls!
            }
        }
    }
    
    
    
    func setup() {
        if self.subLabel.tag != 100 {
            self.subLabel = UILabel(frame: self.bounds)
            self.subLabel.tag = 100
            self.subLabel.text = super.text
            self.subLabel.font = super.font
            self.subLabel.textColor = super.textColor
            self.subLabel.backgroundColor = super.backgroundColor ?? UIColor.clearColor()
            self.subLabel.shadowColor = super.shadowColor
            self.subLabel.shadowOffset = super.shadowOffset
            self.subLabel.layer.anchorPoint = CGPoint.zero
            super.textColor = UIColor.clearColor()
            super.clipsToBounds = true
            super.numberOfLines = 1
            self.addSubview(self.subLabel)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgLabel.restartLabel), name: UIApplicationDidBecomeActiveNotification, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(DevgLabel.shutdownLabel), name: UIApplicationDidEnterBackgroundNotification, object: nil)
            self.updateLabel()
        }
    }
    
    func updateLabel() {
        let expectedLabelSize = self.subLabelSize()
        invalidateIntrinsicContentSize()
        self.maskLayer?.removeAllAnimations()
        self.subLabel.layer.removeAllAnimations()
        
        if self.labelShouldScroll() == false {
            self.subLabel.textAlignment = super.textAlignment
            self.subLabel.lineBreakMode = super.lineBreakMode
            let labelFrame: CGRect = CGRectIntegral(CGRectMake(0.0, 0.0, bounds.size.width, bounds.size.height))
            self.subLabelFrame = labelFrame
            self.awayOffset = 0.0
            self.repliLayer.instanceCount = 1;
            self.subLabel.frame = labelFrame
            
            self.applyGradientMask(self.scrollFadeLength, animated: true)
            
            return
        }
        
        self.speed = .Duration(self.scrollSpeed)
        
        let minTrailing = max(max(0.0, 0.0), self.scrollFadeLength)
        
        self.subLabelFrame = CGRectIntegral(CGRectMake(0.0, 0.0, expectedLabelSize.width, bounds.size.height))
        self.awayOffset = -(self.subLabelFrame.size.width + minTrailing)
        self.subLabel.frame = self.subLabelFrame
        self.repliLayer.instanceCount = 2
        self.repliLayer.instanceTransform = CATransform3DMakeTranslation(-self.awayOffset, 0.0, 0.0)
        
        self.animationDuration = {
            switch self.speed {
            case .Rate(let rate):
                return CGFloat(fabs(self.awayOffset) / rate)
            case .Duration(let duration):
                return duration
            }
        }()
        self.applyGradientMask(self.scrollFadeLength, animated: true)
        self.scrollContinuous(self.animationDuration, delay: 0)
    }
    
    private func scrollContinuous(interval: CGFloat, delay: CGFloat) {
        let homeOrigin = self.subLabelFrame.origin
        let awayOrigin = CGPointMake(self.subLabelFrame.origin.x + self.awayOffset, self.subLabelFrame.origin.y)
        let scroller = Scroller(generator: { (interval: CGFloat, delay: CGFloat) -> [(layer: CALayer, anim: CAKeyframeAnimation)] in
            let values: [NSValue] = [NSValue(CGPoint: homeOrigin), NSValue(CGPoint: homeOrigin), NSValue(CGPoint: awayOrigin)]
            let layer = self.subLabel.layer
            let animation = CAKeyframeAnimation(keyPath: "position")
            let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
            let totalDuration: CGFloat
            totalDuration = delay + interval
            animation.keyTimes = [0.0, delay/totalDuration, 1.0]
            animation.timingFunctions = [timingFunction,timingFunction]
            animation.values = values
            return [(layer: layer, anim: animation)]
        })
        self.scroll(interval, delay: delay, scroller: scroller, fader: nil)
    }
    
    private func scroll(interval: CGFloat, delay: CGFloat, scroller: Scroller, fader: CAKeyframeAnimation?) {
        var scroller = scroller
        if self.superview == nil {
            return
        }
        if self.window == nil {
            return
        }
        let viewController = self.traverseResponderChainForFirstViewController()
        if viewController != nil {
            if !viewController!.isViewLoaded() {
                return
            }
        }
        
        CATransaction.begin()
        let speed = ((self.subLabelSize().width / 10) / interval)
        let transDuration = NSTimeInterval(speed)
        CATransaction.setAnimationDuration(transDuration)
        
        let gradientAnimation: CAKeyframeAnimation?
        if self.scrollFadeLength > 0.0 {
            if let setupAnim = self.maskLayer?.animationForKey("setupFade") as? CABasicAnimation, finalColors = setupAnim.toValue as? [CGColorRef] {
                self.maskLayer?.colors = finalColors
            }
            self.maskLayer?.removeAnimationForKey("setupFade")
            if let previousAnimation = fader {
                gradientAnimation = previousAnimation
            } else {
                gradientAnimation = self.keyFrameAnimationForGradient(self.scrollFadeLength, interval: interval, delay: delay)
            }
            self.maskLayer?.addAnimation(gradientAnimation!, forKey: "gradient")
        } else {
            gradientAnimation = nil
        }
        
        let completion = CompletionBlock<MLAnimationCompletion>({ (finished: Bool) -> () in
            guard finished else {
                return
            }
            guard self.window != nil else {
                return
            }
            guard self.subLabel.layer.animationForKey("position") == nil else {
                return
            }
            if self.labelShouldScroll() == true {
                self.scroll(interval, delay: delay, scroller: scroller, fader: gradientAnimation)
            }
        })
        let scrolls = scroller.generate(interval, delay: delay)
        for (index, scroll) in scrolls.enumerate() {
            let layer = scroll.layer
            let anim = scroll.anim
            if index == 0 {
                anim.setValue(completion as AnyObject, forKey: "MLAnimationCompletion")
                anim.delegate = self
            }
            layer.addAnimation(anim, forKey: "position")
        }
        CATransaction.commit()
        
    }
    
    private func keyFrameAnimationForGradient(fadeLength: CGFloat, interval: CGFloat, delay: CGFloat) -> CAKeyframeAnimation {
        let values: [[CGColorRef]]
        let keyTimes: [CGFloat]
        let transp = UIColor.clearColor().CGColor
        let opaque = UIColor.blackColor().CGColor
        let animation = CAKeyframeAnimation(keyPath: "colors")
        let timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        let totalDuration = delay + interval
        let offsetDistance = self.awayOffset
        let startFadeFraction = fabs((self.subLabel.bounds.size.width) / offsetDistance)
        let startFadeTimeFraction = timingFunction.durationPercentageForPositionPercentage(startFadeFraction, duration: totalDuration)
        let startFadeTime = delay + CGFloat(startFadeTimeFraction) * interval
        
        keyTimes = [0.0, delay/totalDuration, (delay + 0.2)/totalDuration, startFadeTime/totalDuration, (startFadeTime + 0.1)/totalDuration, 1.0]
        
        let mask = self.maskLayer?.presentationLayer() as? CAGradientLayer
        let currentValues = mask?.colors as? [CGColorRef]
        
        values = [currentValues ?? [opaque, opaque, opaque, transp], [opaque, opaque, opaque, transp], [transp, opaque, opaque, transp], [transp, opaque, opaque, transp], [opaque, opaque, opaque, transp], [opaque, opaque, opaque, transp]]
        
        animation.values = values
        animation.keyTimes = keyTimes
        animation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        
        return animation
    }
    
    override public func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if anim is CABasicAnimation {
            if let setupAnim = self.maskLayer?.animationForKey("setupFade") as? CABasicAnimation, finalColors = setupAnim.toValue as? [CGColorRef] {
                self.maskLayer?.colors = finalColors
            }
            self.maskLayer?.removeAnimationForKey("setupFade")
        } else {
            if let completion = anim.valueForKey("MLAnimationCompletion") as? CompletionBlock<MLAnimationCompletion> {
                completion.f(finished: flag)
            }
        }
    }
    
    private func subLabelSize() -> CGSize {
        let maximumLabelSize = CGSizeMake(CGFloat.max, CGFloat.max)
        var expectedLabelSize = self.sizeThatFits(maximumLabelSize)
        expectedLabelSize.width = min(expectedLabelSize.width, 5461.0)
        expectedLabelSize.height = bounds.size.height
        return expectedLabelSize
    }
    
    private func labelShouldScroll() -> Bool {
        if self.subLabel.text == nil {
            return false
        }
        if self.subLabel.text!.isEmpty {
            return false
        }
        
        let labelTooLarge = (self.subLabelSize().width) > self.bounds.size.width
        let animationHasDuration = self.speed.value > 0.0
        return (labelTooLarge && animationHasDuration)
    }
    
    private func applyGradientMask(fadeLength: CGFloat, animated: Bool) {
        self.maskLayer?.removeAllAnimations()
        
        if (fadeLength <= 0.0) {
            self.layer.mask = nil
            return
        }
        
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        let gradientMask: CAGradientLayer
        if let currentMask = self.maskLayer {
            gradientMask = currentMask
        } else {
            gradientMask = CAGradientLayer()
            gradientMask.shouldRasterize = true
            gradientMask.rasterizationScale = UIScreen.mainScreen().scale
            gradientMask.startPoint = CGPointMake(0.0, 0.5)
            gradientMask.endPoint = CGPointMake(1.0, 0.5)
            
            let leftFadeStop = fadeLength/self.bounds.size.width
            let rightFadeStop = fadeLength/self.bounds.size.width
            gradientMask.locations = [0.0, leftFadeStop, (1.0 - rightFadeStop), 1.0]
        }
        
        
        let transparent = UIColor.clearColor().CGColor
        let opaque = UIColor.blackColor().CGColor
        
        
        self.layer.mask = gradientMask
        
        gradientMask.bounds = self.layer.bounds
        gradientMask.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds))
        
        let adjustedColors: [CGColorRef]
        let trailingFadeNeeded = self.labelShouldScroll()
        adjustedColors = [opaque, opaque, opaque, (trailingFadeNeeded ? transparent : opaque)]
        
        if (animated) {
            CATransaction.commit()
            let colorAnimation = CABasicAnimation(keyPath: "colors")
            colorAnimation.fromValue = gradientMask.colors
            colorAnimation.toValue = adjustedColors
            colorAnimation.fillMode = kCAFillModeForwards
            colorAnimation.removedOnCompletion = false
            colorAnimation.delegate = self
            gradientMask.addAnimation(colorAnimation, forKey: "setupFade")
        } else {
            gradientMask.colors = adjustedColors
            CATransaction.commit()
        }
    }
    
    public func shutdownLabel() {
        if self.scrollEnabled == true {
            self.maskLayer?.removeAllAnimations()
            self.subLabel.layer.removeAllAnimations()
            applyGradientMask(self.scrollFadeLength, animated: false)
        }
    }
    
    
    public func restartLabel() {
        if self.scrollEnabled == true {
            self.shutdownLabel()
            self.scrollContinuous(self.animationDuration, delay: 0)
        }
    }
    
    public func pauseLabel() {
        if self.scrollEnabled == true {
            var awayFromHome: Bool = true
            if let presentationLayer = self.subLabel.layer.presentationLayer() as? CALayer {
                awayFromHome = !(presentationLayer.position.x == self.subLabelFrame.origin.x)
            }
            if self.isPaused == true && awayFromHome == false {
                return
            }
            
            let labelPauseTime = self.subLabel.layer.convertTime(CACurrentMediaTime(), fromLayer: nil)
            self.subLabel.layer.speed = 0.0
            self.subLabel.layer.timeOffset = labelPauseTime
            
            let gradientPauseTime = self.maskLayer?.convertTime(CACurrentMediaTime(), fromLayer:nil)
            self.maskLayer?.speed = 0.0
            self.maskLayer?.timeOffset = gradientPauseTime!
        }
    }
    
    public func reuseLabel() {
        if self.scrollEnabled == true {
            if self.isPaused == false {
                return
            }
            
            let labelPauseTime = self.subLabel.layer.timeOffset
            self.subLabel.layer.speed = 1.0
            self.subLabel.layer.timeOffset = 0.0
            self.subLabel.layer.beginTime = 0.0
            self.subLabel.layer.beginTime = self.subLabel.layer.convertTime(CACurrentMediaTime(), fromLayer: nil) - labelPauseTime
            
            let gradientPauseTime = self.maskLayer?.timeOffset
            self.maskLayer?.speed = 1.0
            self.maskLayer?.timeOffset = 0.0
            self.maskLayer?.beginTime = 0.0
            self.maskLayer?.beginTime = self.maskLayer!.convertTime(CACurrentMediaTime(), fromLayer:nil) - gradientPauseTime!
        }
    }
}

private extension CAMediaTimingFunction {
    func durationPercentageForPositionPercentage(positionPercentage: CGFloat, duration: CGFloat) -> CGFloat {
        let controlPoints = self.controlPoints()
        let epsilon: CGFloat = 1.0 / (100.0 * CGFloat(duration))
        let tFound = self.solveTforY(positionPercentage, epsilon: epsilon, controlPoints: controlPoints)
        let durationPercentage = self.XforCurveAt(tFound, controlPoints: controlPoints)
        
        return durationPercentage
    }
    
    func solveTforY(y0: CGFloat, epsilon: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        var t0 = y0
        var t1 = y0
        var f0, df0: CGFloat
        
        for _ in 0..<15 {
            t0 = t1
            f0 = self.YforCurveAt(t0, controlPoints:controlPoints) - y0
            if (fabs(f0) < epsilon) {
                return t0
            }
            df0 = self.derivativeCurveYValueAt(t0, controlPoints:controlPoints)
            if (fabs(df0) < 1e-6) {
                break
            }
            t1 = t0 - f0/df0
        }
        return t0
    }
    
    func YforCurveAt(t: CGFloat, controlPoints:[CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        let y0 = (pow((1.0 - t),3.0) * P0.y)
        let y1 = (3.0 * pow(1.0 - t, 2.0) * t * P1.y)
        let y2 = (3.0 * (1.0 - t) * pow(t, 2.0) * P2.y)
        let y3 = (pow(t, 3.0) * P3.y)
        
        return y0 + y1 + y2 + y3
    }
    
    func XforCurveAt(t: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        let x0 = (pow((1.0 - t),3.0) * P0.x)
        let x1 = (3.0 * pow(1.0 - t, 2.0) * t * P1.x)
        let x2 = (3.0 * (1.0 - t) * pow(t, 2.0) * P2.x)
        let x3 = (pow(t, 3.0) * P3.x)
        
        return x0 + x1 + x2 + x3
    }
    
    func derivativeCurveYValueAt(t: CGFloat, controlPoints: [CGPoint]) -> CGFloat {
        let P0 = controlPoints[0]
        let P1 = controlPoints[1]
        let P2 = controlPoints[2]
        let P3 = controlPoints[3]
        
        let dy0 = (P0.y + 3.0 * P1.y + 3.0 * P2.y - P3.y) * -3.0
        let dy1 = t * (6.0 * P0.y + 6.0 * P2.y)
        let dy2 = (-3.0 * P0.y + 3.0 * P1.y)
        
        return dy0 * pow(t, 2.0) + dy1 + dy2
    }
    
    func controlPoints() -> [CGPoint] {
        var point: [Float] = [0.0, 0.0]
        var pointArray = [CGPoint]()
        for i in 0...3 {
            self.getControlPointAtIndex(i, values: &point)
            pointArray.append(CGPoint(x: CGFloat(point[0]), y: CGFloat(point[1])))
        }
        
        return pointArray
    }
}

private class CompletionBlock<T> {
    let f : T
    init (_ f: T) { self.f = f }
}

private extension UIResponder {
    func traverseResponderChainForFirstViewController() -> UIViewController? {
        if let nextResponder = self.nextResponder() {
            if nextResponder.isKindOfClass(UIViewController) {
                return nextResponder as? UIViewController
            }
            else if (nextResponder.isKindOfClass(UIView)) {
                return nextResponder.traverseResponderChainForFirstViewController()
            }
            else {
                return nil
            }
        }
        return nil
    }
}