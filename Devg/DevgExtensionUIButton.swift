//
//  DevgExtensionUIButton.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public extension UIButton {
    public func indexPathAt(tableView tableView: UITableView) -> NSIndexPath? {
        let position: CGPoint = self.convertPoint(CGPointZero, toView: tableView)
        
        return tableView.indexPathForRowAtPoint(position)
    }
    
    public func indexPathAt(collectionView collectionView: UICollectionView) -> NSIndexPath? {
        let position: CGPoint = self.convertPoint(CGPointZero, toView: collectionView)
        
        return collectionView.indexPathForItemAtPoint(position)
    }
}