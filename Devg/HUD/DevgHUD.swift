//
//  DevgHUD.swift
//  Devg
//
//  Created by Kwan on 2015. 12. 1..
//  Copyright (c) 2014년 Kwan. All rights reserved.
//

import UIKit

public class DevgHUD {
    private var _initHUD: DevgHUDViewController!
    private var initHUD: DevgHUDViewController! {
        get {
            if(self._initHUD == nil) {
                self._initHUD = DevgHUDViewController()
            }
            return self._initHUD
        }
    }
    
    private static var _sharedInstance: DevgHUD!
    public static var sharedInstance: DevgHUD {
        get {
            if(self._sharedInstance == nil) {
                self._sharedInstance = DevgHUD()
            }
            return self._sharedInstance
        }
    }
    
    
    public func show() {
        self.initHUD.titleString = "Loading..."
        self.initHUD.show()
    }
    public func show(title: String) {
        self.initHUD.titleString = title
        self.initHUD.show()
    }
    
    public func hide(animate animate: Bool = true) {
        self.initHUD.hide(animate: animate)
    }
    public func hide(delay timeInterval: NSTimeInterval) {
        NSTimer.scheduledTimerWithTimeInterval(timeInterval, target: self, selector: #selector(DevgHUD._timerBlock(_:)), userInfo: nil, repeats: false)
    }
    
    @objc private func _timerBlock(timer: NSTimer!) {
        self.initHUD.hide(animate: true)
    }
}

extension DevgHUD {
    
}

class DevgHUDViewController: UIViewController {
    @IBOutlet weak var contentsView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var titleLabelView: UILabel!
    
    var titleString = "Loading..."
    
    convenience init() {
        self.init(nibName: "DevgHUD", bundle: NSBundle(identifier: "net.devg"))
    }
    
    private func show() -> Void {
        let window: UIWindow = UIApplication.sharedApplication().delegate!.window!!
        
        self.view.frame.origin.x = 0.0
        self.view.frame.origin.y = 0.0
        self.view.frame.size.width = window.bounds.size.width
        self.view.frame.size.height = window.bounds.size.height
        
        self.activityIndicator.transform = CGAffineTransformMakeScale(1.25, 1.25)
        
        //self.view.alpha = 0.0
        Devg.visibleViewController().view.addSubview(self.view)
        
        self.viewDidLoad()
        
        UIView.animateWithDuration(0.25, animations: {() -> Void in
            self.view.alpha = 1.0
            
            }, completion: {(complete: Bool) -> Void in
                if(self.activityIndicator != nil) {
                    self.activityIndicator.startAnimating()
                }
        })
    }
    
    private func hide(animate animate: Bool) -> Void {
        if(self.activityIndicator != nil) {
            self.activityIndicator.stopAnimating()
        }
        
        if(animate) {
            UIView.animateWithDuration(0.75, animations: {() -> Void in
                self.view.alpha = 0.0
                
                }, completion: {(complete: Bool) -> Void in
                    self.view.removeFromSuperview()
            })
        } else {
            self.view.alpha = 0.0
            self.view.removeFromSuperview()
        }
    }
    
    internal override func viewDidLoad() {
        self.contentsView.layer.cornerRadius  = 12.0
        self.contentsView.layer.masksToBounds = true
        self.contentsView.layer.borderColor   = UIColor.clearColor().CGColor
        self.contentsView.layer.borderWidth   = 0.0
        
        self.titleLabelView.text = self.titleString
        
        let blurEffect = UIBlurEffect(style: .Light)
        let blurredBackground = UIVisualEffectView(effect: blurEffect)
        //blurredBackground.alpha = 0.75
        blurredBackground.frame = self.contentsView.bounds
        blurredBackground.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        self.contentsView.addSubview(blurredBackground)
        self.contentsView.sendSubviewToBack(blurredBackground)
        self.contentsView.backgroundColor = .clearColor()
    }
    
    internal override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    internal override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
}