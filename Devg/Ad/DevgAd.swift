//
//  DevgAd.swift
//  Devg
//
//  Created by Lee JinWook on 2016. 4. 21..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation
import AdSupport
import Devg

public class DevgAd: NSObject {
    public class func uuid() -> String {
        var uuid: String?
        let keyName: String = "AdUUID"
        
        let SecMatchLimit: String = kSecMatchLimit as String
        let SecReturnData: String = kSecReturnData as String
        let SecValueData: String = kSecValueData as String
        let SecAttrAccessible: String = kSecAttrAccessible as String
        let SecClass: String = kSecClass as String
        let SecAttrService: String = kSecAttrService as String
        let SecAttrGeneric: String = kSecAttrGeneric as String
        let SecAttrAccount: String = kSecAttrAccount as String
        
        var readKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
        readKeychainQuery[SecAttrService] = Devg.bundelID()
        let encodedIdentifier: NSData? = keyName.dataUsingEncoding(NSUTF8StringEncoding)
        readKeychainQuery[SecAttrGeneric] = encodedIdentifier
        readKeychainQuery[SecAttrAccount] = encodedIdentifier
        
        var result: AnyObject?
        readKeychainQuery[SecMatchLimit] = kSecMatchLimitOne
        readKeychainQuery[SecReturnData] = kCFBooleanTrue
        let status = withUnsafeMutablePointer(&result) {
            SecItemCopyMatching(readKeychainQuery, UnsafeMutablePointer($0))
        }
        let keychainData: NSData? = status == noErr ? result as? NSData : nil
        var stringValue: String?
        if let data = keychainData {
            stringValue = NSString(data: data, encoding: NSUTF8StringEncoding) as String?
        }
        
        uuid = stringValue
        
        if uuid == nil { // create
            let newUuid: String = ASIdentifierManager.sharedManager().advertisingIdentifier.UUIDString
            
            var saveKeychainQuery: [String: AnyObject] = [SecClass:kSecClassGenericPassword]
            saveKeychainQuery[SecAttrService] = Devg.bundelID()
            let encodedIdentifier: NSData? = keyName.dataUsingEncoding(NSUTF8StringEncoding)
            saveKeychainQuery[SecAttrGeneric] = encodedIdentifier
            saveKeychainQuery[SecAttrAccount] = encodedIdentifier
            
            saveKeychainQuery[SecValueData] = newUuid.dataUsingEncoding(NSUTF8StringEncoding)
            saveKeychainQuery[SecAttrAccessible] = kSecAttrAccessibleWhenUnlocked
            
            uuid = newUuid
        }
        
        return uuid!
    }
}