//
//  DevgAlert.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public class DevgAlert: NSObject {
    public static func show(withViewController viewContoller: UIViewController, title: String? = nil, message: String? = nil, buttons: [String] = ["확인"], destructiveIndex: Int? = nil, handler: ((buttonIndex: Int) -> Void)? = nil) {
        
        let alertController: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        for (index, buttonString) in buttons.enumerate() {
            let actions: UIAlertAction = UIAlertAction(title: buttonString, style: destructiveIndex != index ? .Default:.Destructive, handler: { (action) -> Void in
                handler?(buttonIndex: index)
            })
            alertController.addAction(actions)
        }
        viewContoller.presentViewController(alertController, animated: true, completion: nil)
    }
}
