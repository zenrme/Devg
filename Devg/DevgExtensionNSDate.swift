//
//  DevgExtensionNSDate.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public extension NSDate {
    /// Date Compare
    ///
    /// :param: 비교할 Date
    /// :returns: Bool
    public func compareDate(to: NSDate? = nil, from: NSDate? = nil) -> Bool {
        var compare: Bool = false
        if let toDate: NSDate = to {
            if self.compare(toDate) == .OrderedDescending {
                compare = true
            }
        }
        
        if let fromDate: NSDate = from {
            if self.compare(fromDate) == .OrderedAscending {
                compare = true
            }
        }
        
        return compare
    }
    
    /// local Date
    ///
    /// :returns: NSDate
    public func localFromDate() -> NSDate {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .FullStyle
        dateFormatter.timeStyle = .FullStyle
        return dateFormatter.dateFromString(dateFormatter.stringFromDate(self))!
    }
    
    /// gmt Date
    ///
    /// :returns: NSDate
    public func gmtFromDate() -> NSDate {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .FullStyle
        dateFormatter.timeStyle = .FullStyle
        let timeZomeOffset: NSTimeInterval = NSTimeInterval(NSTimeZone.systemTimeZone().secondsFromGMTForDate(self))
        return dateFormatter.dateFromString(dateFormatter.stringFromDate(self.dateByAddingTimeInterval(-timeZomeOffset)))!
    }
    
    /// Date to String
    ///
    /// :param: 포멧 String
    /// :returns: Date String
    public func toString(format: String) -> String {
        let dateFormatter: NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.stringFromDate(self)
    }
}