//
//  DevgOnce.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public class DevgOnce: NSObject {
    class var sharedInstance : DevgOnce {
        struct Static {
            static var token : dispatch_once_t = 0
            static var instance : DevgOnce? = nil
        }
        dispatch_once(&Static.token) {
            Static.instance = DevgOnce()
        }
        return Static.instance!
    }
    
    public class func run(action: () -> Void) {
        DevgOnce.sharedInstance.run(action)
    }
    
    var isFinish: Bool = true
    func run(action: () -> Void) {
        if self.isFinish == true {
            self.isFinish = false
            action()
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC))), dispatch_get_main_queue()) {
                self.isFinish = true
            }
        }
    }
}
