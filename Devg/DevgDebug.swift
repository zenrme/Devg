//
//  DevgDebug.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import Foundation

public func l(log: Any!, prefix: String = "LOG", file: String = #file, line: Int = #line, function: String = #function) {
    if isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        var existContents: String = ""
        if Devg.existFile("log.txt", type: .Cache) {
            if let string: String = Devg.getContents("log.txt", type: .Cache) {
                existContents = string
            }
        }
        Devg.createFile("log.txt", contents: existContents + "# " + timestamp() + "\n" + string, type: .Cache)
        print(string)
    }
}

public func d(log: Any!, prefix: String = "DEBUG", file: String = #file, line: Int = #line, function: String = #function) {
    if isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        print(string)
    }
}

public func e(log: Any!, prefix: String = "ERROR", file: String = #file, line: Int = #line, function: String = #function) {
    if isNotFromAppStore() {
        let string: String = debugString(log, prefix: prefix, file: file, line: line, function: function)
        var existContents: String = ""
        if Devg.existFile("err.txt", type: .Cache) {
            if let string: String = Devg.getContents("err.txt", type: .Cache) {
                existContents = string
            }
        }
        Devg.createFile("err.txt", contents: existContents + "# " + timestamp() + "\n" + string, type: .Cache)
        print(string)
    }
}

func debugString(log: Any!, prefix: String = "LOG", file: String = #file, line: Int = #line, function: String = #function) -> String {
    let lastFile: String = (file as NSString).lastPathComponent
    return "\n[\(prefix):] \(function) in \(lastFile)(\(line))\n\(log)\n[:\(prefix)] \(function) in \(lastFile)(\(line))\n"
}

func timestamp() -> String {
    let timestamp: String = NSDateFormatter.localizedStringFromDate(NSDate(), dateStyle: .ShortStyle, timeStyle: .ShortStyle)
    return timestamp
}

var mobileProvision: NSDictionary!
func isNotFromAppStore() -> Bool {
    if TARGET_OS_SIMULATOR != 0 || TARGET_IPHONE_SIMULATOR != 0 {
        return true
    }
    else if (mobileProvision == nil) {
        let provisioningPath: String! = NSBundle.mainBundle().pathForResource("embedded", ofType: "mobileprovision")
        if (provisioningPath == nil) {
            return false
        }
        
        let binaryString: String
        do {
            let string: String = try String(contentsOfFile: provisioningPath, encoding: NSISOLatin1StringEncoding)
            binaryString = string
        }
        catch {
            return false
        }
        
        let scanner: NSScanner = NSScanner(string: binaryString)
        var ok: Bool = scanner.scanUpToString("<plist", intoString: nil)
        if !ok {
            return false
        }
        var plistString: NSString?
        ok = scanner.scanUpToString("</plist>", intoString: &plistString)
        if !ok {
            return false
        }
        plistString = NSString(format: "%@</plist>", plistString!)
        
        if let plistdata_latin1: NSData  = plistString?.dataUsingEncoding(NSISOLatin1StringEncoding) {
            do {
                mobileProvision = try NSPropertyListSerialization.propertyListWithData(plistdata_latin1, options: NSPropertyListReadOptions.Immutable, format: nil) as! NSDictionary
            }
            catch {
                return false
            }
            
        }
        else {
            return false
        }
    }
    
    if mobileProvision != nil {
        if let _ = mobileProvision["ProvisionedDevices"] {
            return true
        }
    }
    
    return false
}
