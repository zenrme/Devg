//
//  DevgImageOperationQueue.swift
//  Devg
//
//  Created by Kwan on 2016. 5. 9..
//  Copyright © 2016년 Kwan. All rights reserved.
//

import UIKit

public class DevgImageOperationQueue: NSObject {
    public enum ResizeType: Int {
        case None
        case AspectFit
        case AspectFill
        case WidthFit
    }
    
    var imageDownloadOperations: [String: [String: NSBlockOperation]] = Dictionary<String, Dictionary<String, NSBlockOperation>>()
    var imageOperation: NSOperationQueue = NSOperationQueue()
    
    public class var sharedInstance : DevgImageOperationQueue {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : DevgImageOperationQueue? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DevgImageOperationQueue()
        }
        return Static.instance!
    }
    
    public func loadImage(imageView: UIImageView, imageUrlString: String, resizeType: ResizeType? = nil, defaultImage: UIImage = UIImage(), indexPath: NSIndexPath, file: String = #file, completionHandler: ((image: UIImage) -> Void)? = nil, errorHandler: ((error: NSError?) -> Void)? = nil) {
        let lastFile: String = (file as NSString).lastPathComponent
        var image: UIImage = defaultImage
        if self.checkURL(imageUrlString) {
            var fileName: String
            if let string: String = imageUrlString.toURLEncode(.Full) {
                fileName = string.toBase64()
            }
            else {
                fileName = imageUrlString.toBase64()
            }
            var isCached: Bool = false
            if Devg.existFile(fileName, type: .Cache) {
                if let cacheImage: UIImage = UIImage(contentsOfFile: Devg.filePath(fileName, type: .Cache)) {
                    image = cacheImage
                    isCached = true
                }
            }
            
            var newImage: UIImage? = image
            if let type: ResizeType = resizeType {
                switch type {
                case .AspectFill:
                    newImage = image.aspectFill(imageView.bounds.size)
                    break
                case .AspectFit:
                    newImage = image.aspectFit(imageView.bounds.size)
                    break
                case .WidthFit:
                    newImage = image.widthFit(imageView.frame.size.width)
                default:
                    break
                }
            }
            
            imageView.image = newImage
            
            if isCached {
                completionHandler?(image: newImage!)
            }
            else {
                let loadImageIntoCellOp: NSBlockOperation = NSBlockOperation()
                loadImageIntoCellOp.addExecutionBlock({ () -> Void in
                    if let url: NSURL = NSURL(string: imageUrlString) {
                        if let imageData: NSData = NSData(contentsOfURL: url) {
                            if let uiimage: UIImage = UIImage(data: imageData) {
                                if let data: NSData = UIImagePNGRepresentation(uiimage) {
                                    data.writeToFile(Devg.filePath(fileName, type: .Cache), atomically: true)
                                }
                                NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                                    if loadImageIntoCellOp.cancelled == false {
                                        newImage = uiimage
                                        if let type: ResizeType = resizeType {
                                            switch type {
                                            case .AspectFill:
                                                newImage = uiimage.aspectFill(imageView.bounds.size)
                                                break
                                            case .AspectFit:
                                                newImage = uiimage.aspectFit(imageView.bounds.size)
                                                break
                                            case .WidthFit:
                                                newImage = uiimage.widthFit(imageView.frame.size.width)
                                            default:
                                                break
                                            }
                                        }
                                        
                                        imageView.image = newImage
                                        completionHandler?(image: newImage!)
                                    }
                                    
                                    if self.imageDownloadOperations[lastFile] != nil {
                                        self.imageDownloadOperations[lastFile]!.removeValueForKey("\(indexPath.section)+\(indexPath.row)")
                                    }
                                })
                            }
                            else {
                                imageView.image = defaultImage
                                let error = NSError(domain: Devg.bundelID(), code: 415, userInfo: ["URL": imageUrlString])
                                errorHandler?(error: error)
                            }
                        }
                        else {
                            imageView.image = defaultImage
                            let error = NSError(domain: Devg.bundelID(), code: 415, userInfo: ["URL": imageUrlString])
                            errorHandler?(error: error)
                        }
                    }
                    else {
                        imageView.image = defaultImage
                        let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": imageUrlString])
                        errorHandler?(error: error)
                    }
                })
                
                if self.imageDownloadOperations[lastFile] != nil {
                    self.imageDownloadOperations[lastFile]!["\(indexPath.section)+\(indexPath.row)"] = loadImageIntoCellOp
                }
                else {
                    self.imageDownloadOperations[lastFile] = ["\(indexPath.section)+\(indexPath.row)": loadImageIntoCellOp]
                }
                self.imageOperation.addOperation(loadImageIntoCellOp)
            }
        }
        else {
            imageView.image = defaultImage
            let error = NSError(domain: Devg.bundelID(), code: 404, userInfo: ["URL": imageUrlString])
            errorHandler?(error: error)
        }
    }
    
    public func removeOperationQueue(indexPath: NSIndexPath, file: String = #file) {
        let lastFile: String = (file as NSString).lastPathComponent
        if let ongoingDownloadOperation: NSBlockOperation = self.imageDownloadOperations[lastFile]?["\(indexPath.section)+\(indexPath.row)"] {
            ongoingDownloadOperation.cancel()
            self.imageDownloadOperations[lastFile]!.removeValueForKey("\(indexPath.section)+\(indexPath.row)")
        }
    }
    
    public func removeAllOperationQueues(file: String = #file) {
        let lastFile: String = (file as NSString).lastPathComponent
        if self.imageDownloadOperations[lastFile] != nil {
            for (key, queue) in self.imageDownloadOperations[lastFile]! {
                queue.cancel()
                self.imageDownloadOperations[lastFile]!.removeValueForKey(key)
            }
        }
    }
    
    func checkURL(urlString: String?) -> Bool {
        if let urlString = urlString {
            if NSURL(string: urlString) != nil {
                return true
            }
        }
        return false
    }
}
